<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$arr_lounge_place[1] = "OAZ본점";
	$arr_lounge_place[2] = "OAZ국제거리점";
	
	$arr_bus_num[1] = "1번 11:05출발";
	$arr_bus_num[2] = "2번 13:05출발";
	$arr_bus_num[3] = "3번 14:50출발";
	$arr_bus_num[4] = "4번 17:20출발";
	$arr_bus_num[5] = "5번 18:35출발";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />
<link rel="stylesheet" type="text/css" href="/app_management/css/chosen.min.css" />
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<style type="text/css">
  dl{margin: 0px;}
</style>

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script type="text/javascript">
  $(function() {
    $( '.date_added' ).datepicker( {
        dateFormat: 'yy-mm-dd',
        language: 'ja',
        autoclose: true,
        clearBtn: true,
        numberOfMonths: [1, 3],
    } );
    //$( '.date_added' ).datepicker().datepicker( 'setDate', 'today' );

});
</script>

<script language="javascript"> 
	function fnChangeSel(i, j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './customer_del.php?shop_id='+i+'&customer_id='+j;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("customer_name");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

    $('#s_regi_date_from').change(function() {
      $('#s_regi_date_to').val($('#s_regi_date_from').val());
    });

    $('#s_check_day_from').change(function() {
      $('#s_check_day_to').val($('#s_check_day_from').val());
    });
		
	});
//-->
</script>

</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_admin_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
		
	if($customer_id!="")
	{

		$arr_db_field = array("customer_id", "customer_name", "company_id", "customer_name_en");
		$arr_db_field = array_merge($arr_db_field, array("cate_air_in_id", "check_in", "check_out", "lounge_place", "bus_num"));
		$arr_db_field = array_merge($arr_db_field, array("customer_tel", "customer_comment", "customer_email", "customer_kakao", "customer_years", "pay_money"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
			
		$sql = "SELECT customer_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_customer where customer_id='".$customer_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//カテゴリをArrayへ
	$arr_db_field = array("company_id", "company_title");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_company order by view_level";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_company_title[$db_result[$db_loop]["company_id"]] = $db_result[$db_loop]["company_title"];
		}
	}
	
	$arr_db_field = array("cate_air_in_id", "cate_air_in_name");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_cate_air_in order by view_level";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_air_in_name[$db_result[$db_loop]["cate_air_in_id"]] = $db_result[$db_loop]["cate_air_in_name"];
		}
	}
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_customer.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">

				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>顧客管理</p>
					</div>
				</div>
        <br />
        <div><a href="<?=$_SERVER["PHP_SELF"];?>">顧客新規登録</a></div>
        
        <section id="main" class="clearfix">
        <form action="./customer_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
				<?php $var = "shop_id";?>
        <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<?php echo $$var;?>" />
        <table cellpadding="5" cellspacing="1" class="common_table input_table">
          <tr>
            <th>顧客ID</th>
            <td>
            	<?php if($customer_id=="") {echo "自動生成";} else { echo $customer_id;}?>
            	<input name="customer_id" type="hidden" value="<?php echo $customer_id;?>" />
            </td>
          </tr>
          <tr>
            <th>旅行会社</th>
            <td>
							<?php $var = "company_id";?>
              <select name="<?=$var;?>" id="<?=$var;?>" class="chosen">
                <option value="" selected>---</option>
              <? 
              foreach($arr_company_title as $key => $value)
              {
              ?>
                <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>顧客名</th>
            <td>
							<?php $var = "customer_name";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>顧客名(英語)</th>
            <td>
							<?php $var = "customer_name_en";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>電話番号</th>
            <td>
							<?php $var = "customer_tel";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>飛行機スケジュール(IN)</th>
            <td>
							<?php $var = "cate_air_in_id";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_cate_air_in_name as $key => $value)
              {
              ?>
                <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>셔틀번호</th>
            <td>
							<?php $var = "bus_num";?>
              <? if($$var!=""){ echo $$var."<br />";}?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_bus_num as $key => $value)
              {
              ?>
                <option<? if ($$var == $value)  { echo " selected";}?> value="<?=$value;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>카드수령장소</th>
            <td>
							<?php $var = "lounge_place";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_lounge_place as $key => $value)
              {
              ?>
                <option<? if ($$var == $key) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>CHECK IN</th>
            <td>
							<?php $var = "check_in";?>
              <? if($$var == "0000-00-00") {$$var = "";}?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="date_added" />
              <br />例）<? echo date("Y-m-d");?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>CHECK OUT</th>
            <td>
							<?php $var = "check_out";?>
              <? if($$var == "0000-00-00") {$$var = "";}?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="date_added" />
              <br />例）<? echo date("Y-m-d");?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>メール</th>
            <td>
							<?php $var = "customer_email";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>KAKAO TALK ID</th>
            <td>
							<?php $var = "customer_kakao";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>생년<br />트래블 패키지용</th>
            <td>
              <?php $var = "customer_years";?>
              <select name="<?php echo $var;?>" id="<?php echo $var;?>" class="inp_sel" >
                <option value="">--</option>
                <?
                  for($loop=date("Y") ; $loop>1920 ; $loop--)
                  {
                ?>
                <option value="<? echo $loop;?>" <? if($loop==$$var){ echo " selected ";}?>><? echo $loop;?></option>
                <?
                    
                  }
                ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>コメント</th>
            <td>
							<?php $var = "customer_comment";?>
              <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" style="height:100px;"><?php echo $$var;?></textarea>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>清算金額</th>
            <td>
							<?php $var = "pay_money";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ステータス</th>
            <td>
            	<?php $var = "flag_open";?>
            	<?php if($$var == "") {$$var = "1";}?>
              <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="1" <? if($flag_open==1){echo "checked" ;}?>> 未清算　</label>　
              <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="0" <? if($flag_open!=1){echo "checked" ;}?>> 清算済</label>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button input_table_confirm">
          <tr>
            <td>
              <input type="submit" value="この内容で作成" id="form_confirm">
            </td>
          </tr>
        </table>
        </form>
        </section>
        <br />
      
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>キーワード</th>
            <td>
							<?php $var = "s_keyword";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
          <tr>
            <th>CHECK IN</th>
            <td>
							<?php $var = "s_check_in";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="date_added" />
              <br />例）<? echo date("Y-m-d");?>
            </td>
          </tr>
          <tr>
            <th>滞在日</th>
            <td>
              <?php $var = "s_check_day_from";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="date_added w100" />
              〜
              <?php $var = "s_check_day_to";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="date_added w100" />
              <br />例）<? echo date("Y-m-d");?>
            </td>
          </tr>
          <tr>
            <th>登録日</th>
            <td>
              <?php $var = "s_regi_date_from";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="date_added w100" />
              〜
              <?php $var = "s_regi_date_to";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" class="date_added w100" />
              <br />例）<? echo date("Y-m-d");?>
            </td>
          </tr>
          <tr>
            <th>旅行会社</th>
            <td>
							<?php $var = "s_company_id";?>
              <select name="<?=$var;?>" id="<?=$var;?>" class="chosen">
                <option value="" selected>---</option>
              <? 
              foreach($arr_company_title as $key => $value)
              {
              ?>
                <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
            </td>
          </tr>
          <tr>
            <th>카드수령장소</th>
            <td>
							<?php $var = "s_lounge_place";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_lounge_place as $key => $value)
              {
              ?>
                <option<? if ($$var == $key) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ステータス</th>
            <td>
							<?
								$arr_flag_open[1] = "未清算";
								$arr_flag_open[0] = "清算済";
							?>
							<? $var = "s_flag_open";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_flag_open as $key => $value)
              {
              ?>
                <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?></option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="検索する" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (customer_id like '%".$s_keyword."%' or customer_name like '%".$s_keyword."%' or customer_name_en like '%".$s_keyword."%' or customer_tel like '%".$s_keyword."%' ) ";
	}
	if($s_check_in != "")
	{
		$where .= " and check_in='".$s_check_in."' ";
	}
	if($s_flag_open != "")
	{
		$where .= " and flag_open='".$s_flag_open."' ";
	}
	if($s_company_id != "")
	{
		$where .= " and company_id='".$s_company_id."' ";
	}
  if($s_lounge_place != "")
  {
    $where .= " and lounge_place='".$s_lounge_place."' ";
  }
  if($s_regi_date_from != "" && $s_regi_date_to != "")
  {
    $where .= " and (DATE_FORMAT(regi_date, '%Y-%m-%d')>='".$s_regi_date_from."' and DATE_FORMAT(regi_date, '%Y-%m-%d')<='".$s_regi_date_to."') ";
  }
  if($s_check_day_from != "" && $s_check_day_to != "")
  {
    $where .= " and (DATE_FORMAT(check_in, '%Y-%m-%d')<='".$s_check_day_to."' and DATE_FORMAT(check_out, '%Y-%m-%d')>='".$s_check_day_from."') ";
  }

	

	//合計
	$sql_count = "SELECT count(customer_id) as all_count FROM app_customer where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("customer_id", "customer_name", "company_id", "customer_name_en");
	$arr_db_field = array_merge($arr_db_field, array("cate_air_in_id", "check_in", "check_out", "lounge_place"));
	$arr_db_field = array_merge($arr_db_field, array("customer_tel", "customer_comment", "customer_email", "customer_kakao", "customer_years", "pay_money"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_customer where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
        すべて：<?=$all_count;?>
        <br />
        「総」決済した総金額です。<br />
        「1%は加盟店コミッションからの5%です」(使用額*(加盟店コミッション*0.01))*(0.05) 
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th width="70">編集</th>
            <th>旅行会社</th>
            <th>顧客ID</th>
            <th>顧客名</th>
            <th>英語名</th>
            <th>check in<br />check out</th>
            <th>飛行機<br >in</th>
            <th>수령장소</th>
            <th>金額</th>
            <th>清算有無<br />清算金額</th>
            <th>登録日<br />修正日</th>
          </tr>
          <tr align="center">
            <td>ソート</td>
            <td><a href="?order_name=company_id&order=desc">▼</a>｜<a href="?order_name=company_id">▲</a></td>
            <td><a href="?order_name=customer_id&order=desc">▼</a>｜<a href="?order_name=customer_id">▲</a></td>
            <td><a href="?order_name=customer_name&order=desc">▼</a>｜<a href="?order_name=customer_name">▲</a></td>
            <td><a href="?order_name=customer_name_en&order=desc">▼</a>｜<a href="?order_name=customer_name_en">▲</a></td>
            <td><a href="?order_name=check_in&order=desc">▼</a>｜<a href="?order_name=check_in">▲</a></td>
            <td><a href="?order_name=cate_air_in_id&order=desc">▼</a>｜<a href="?order_name=cate_air_in_id">▲</a></td>
            <td><a href="?order_name=lounge_place&order=desc">▼</a>｜<a href="?order_name=lounge_place">▲</a></td>
            <td></td>
            <td><a href="?order_name=flag_open&order=desc">▼</a>｜<a href="?order_name=flag_open">▲</a></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
	$all_pay_count = 0;
	$all_sum_price = 0;
	
	for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
          <tr align="center">
            <td>
              <a href="?customer_id=<?php echo $customer_id;?>"><img src="/app_management/images/btn_02.gif" alt="編集" /></a>　
              <a href="#" onClick='fnChangeSel("<?php echo $shop_id;?>", "<?php echo $customer_id;?>");'><img src="/app_management/images//btn_03.gif" alt="削除" /></a>
            </td>
            <td><?php echo $arr_company_title[$company_id];?></td>
            <td><?php echo $customer_id;?></td>
            <td><?php echo $customer_name;?></td>
            <td><?php echo $customer_name_en;?></td>
            <td>
              <?php if($check_in!="0000-00-00"){ echo $check_in; }?>
              <?php if($check_out!="0000-00-00"){ echo "<br />".$check_out; }?>
            </td>
            <td><?php echo $arr_cate_air_in_name[$cate_air_in_id];?></td>
            <td><?php echo $arr_lounge_place[$lounge_place];?></td>
            
            <td>
            	<a href="/app_management/shop_uriage/shop_uriage_list.php?s_customer_id=<? echo $customer_id;?>#form_search">
							<?php 
							$sql_customer = "SELECT COUNT( price ) as customer_count, SUM( price ) as customer_price, SUM( (price*(shop_percent*0.01))*(0.05)  ) as customer_price_1 FROM app_shop_uriage WHERE customer_id ='".$customer_id."' and status!=99";
							$db_result_customer = $common_dao->db_query($sql_customer);
							if($db_result_customer)
							{
								echo $db_result_customer[0]["customer_count"]."件<br />";
								echo "総:".number_format($db_result_customer[0]["customer_price"])."円<br />";
								echo "1%→".number_format($db_result_customer[0]["customer_price_1"])."円";
							}
							?>
              </a>
            </td>
            <td>
							<?php
              if($flag_open==1)
              {
                $open_check = "未清算";
              }
              else
              {
                $open_check = "<span style='color:#F00; font-size:15px;'>清算済</span>";
								$all_pay_count++;
								$all_sum_price += $pay_money;
              }
              echo $open_check;
							if($pay_money!="0")
							{
								echo "<br />".number_format($pay_money);
							}
              ?>
            </td>
            <td><?php echo $regi_date."<br />".$up_date;?></td>
          </tr>
          
<?php
	}
?>
          <tr align="center">
          	<td></td>
          	<td style="background-color: rgb(255, 180, 0);" colspan="10">
            	清算済：<?php echo number_format($all_sum_price);?>円（<?php echo number_format($all_pay_count);?>件）　
            </td>
          </tr>
        </table>

        <div style="text-align:center;">
          <?php $common_connect -> Fn_paging($view_count, $all_count); ?>
        </div><!-- //center-->
      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>


  <script src="/app_management/js/chosen.jquery.js"></script>

  <script>
  $(function(){
    $(".chosen").chosen({
      width: "500px",
      search_contains:true // 途中選択
    });
  });
  </script>
</body>
</html>