z１<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($customer_name == "")
	{
		$common_connect -> Fn_javascript_back("名前を入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("customer_name", "company_id", "customer_name_en");
	$arr_db_field = array_merge($arr_db_field, array("cate_air_in_id", "check_in", "check_out", "lounge_place", "bus_num"));
	$arr_db_field = array_merge($arr_db_field, array("customer_tel", "customer_comment", "customer_email", "customer_kakao", "customer_years", "pay_money"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open"));

	//基本情報
	if($customer_id=="")
	{
		$db_insert = "insert into app_customer ( ";
		$db_insert .= " customer_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update app_customer set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where customer_id='".$customer_id."'";
	}
	
	$db_result = $common_dao->db_update($db_insert);
				
	if ($customer_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$customer_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	$common_connect-> Fn_javascript_move("顧客登録・修正しました", "customer_list.php?customer_id=".$customer_id);
?>
</body>
</html>