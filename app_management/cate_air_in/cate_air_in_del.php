<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>飛行機スケジュール(IN)登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$cate_air_in_id = $_GET['cate_air_in_id'];

	//DBへ保存
	if ($cate_air_in_id != "")
	{		
		//商品情報削除
		$dbup = "delete from app_cate_air_in where cate_air_in_id = '$cate_air_in_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("飛行機スケジュール(IN)を削除しました。", "cate_air_in_list.php#search_list");

?>
</body>
</html>