<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>画像削除</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$company_id = $_GET['company_id'];
	$img = $_GET['img'];
	$img_name = $_GET['img_name'];
	
	//DBへ保存
	if ($company_id != "")
	{	
		unlink($_SERVER['DOCUMENT_ROOT']."/".global_company_dir.$company_id."/".$img);
		
		//商品情報削除
		$dbup = "update app_company set $img_name = '' where company_id = '$company_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("ファイルを削除しました。", "company_list.php?company_id=$company_id");

?>
</body>
</html>
