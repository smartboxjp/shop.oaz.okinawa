<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>パトナー登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$company_id = $_GET['company_id'];

	//DBへ保存
	if ($company_id != "")
	{
		//商品情報削除
		$dbup = "delete from app_company where company_id = '$company_id'";
		$db_result = $common_dao->db_update($dbup);
	}
	
	$common_connect -> Fn_javascript_move("パトナーを削除しました。", "company_list.php#search_list");

?>
</body>
</html>