<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>パトナー登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($company_title == "")
	{
		$common_connect -> Fn_javascript_back("タイトルを入力してください。");
	}
	
	if($check_up=="")
	{
		$sql = "select company_login_id from app_company where company_login_id ='$company_login_id'";
	
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			if($company_login_id == $db_result[0]["company_login_id"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているログインIDです。");
			}
		}
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("company_title", "company_percent");
	$arr_db_field = array_merge($arr_db_field, array("company_login_id", "company_login_pw"));
	$arr_db_field = array_merge($arr_db_field, array("view_level", "flag_open"));

	//基本情報
	if($company_id=="")
	{
		$db_insert = "insert into app_company ( ";
		$db_insert .= " company_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update app_company set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where company_id='".$company_id."'";
	}

	$db_result = $common_dao->db_update($db_insert);
				
	if ($company_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$company_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	$db_result = $common_dao->db_update($dbup);
	
	//Folder生成
	$save_dir = $global_path.global_company_dir.$company_id."/";
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $company_id, $text_img_1);
	
	$new_end_name="_2";
	$fname_new_name[2] = $common_image -> img_save("img_2", $common_connect, $save_dir, $new_end_name, $company_id, $text_img_2);
	
	$dbup = "update app_company set ";
	for($loop = 1 ; $loop<3 ; $loop++)
	{
		$dbup .= " img_".$loop."='".$fname_new_name[$loop]."',";
	}
	$dbup .= " up_date='$datetime' where company_id='".$company_id."'";
	
	$db_result = $common_dao->db_update($dbup);


	
	$common_connect-> Fn_javascript_move("パトナー登録・修正しました", "company_list.php?company_id=".$company_id);
?>
</body>
</html>