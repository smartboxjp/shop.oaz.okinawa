<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップ管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->
<script language="javascript"> 
	function fnChangeSel(i) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './shop_del.php?shop_id='+i;
		} 
	}
	
	function fnImgDel(i, j, k) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './img_one_del.php?shop_id='+i+'&img='+j+'&img_name='+k;
		} 
	}
</script>
</head>

<body>
<?
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<!--顧客データ登録メニュー-->
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_shop.php"; ?>
		<!--顧客データ登録メニュー-->
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">

        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>タイトル</th>
            <td>
            	<?php $var = "s_keyword";?>
            	<input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="検索する" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (shop_id like '%".$s_keyword."%' or shop_login_id like '%".$s_keyword."%' or shop_name like '%".$s_keyword."%' or shop_kana like '%".$s_keyword."%' or shop_comment like '%".$s_keyword."%' )";
	}

	
	

	//合計
	$sql_count = "SELECT count(shop_id) as all_count FROM app_shop where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("shop_id", "shop_login_id", "shop_login_pw", "shop_name", "shop_kana", "shop_opentime", "shop_holliday");
	$arr_db_field = array_merge($arr_db_field, array("shop_percent", "shop_comment", "shop_email", "img_1"));
	$arr_db_field = array_merge($arr_db_field, array("tel", "shop_address"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_shop where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
        すべて：<?=$all_count;?>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th width="70">編集</th>
            <th>
            	ショップID
              <br />
              ログインID
            </th>
            <th>画像</th>
            <th>ショップ名</th>
            <th>コミッション</th>
            <th>公開有無</th>
            <th>登録日<br />修正日</th>
          </tr>
          <tr align="center">
            <td>ソート</td>
            <td><a href="?order_name=shop_id&order=desc">▼</a>｜<a href="?order_name=shop_id">▲</a></td>
            <td></td>
            <td><a href="?order_name=shop_name&order=desc">▼</a>｜<a href="?order_name=shop_name">▲</a></td>
            <td><a href="?order_name=shop_percent&order=desc">▼</a>｜<a href="?order_name=shop_percent">▲</a></td>
            <td><a href="?order_name=flag_open&order=desc">▼</a>｜<a href="?order_name=flag_open">▲</a></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
  for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
  {
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
		}
?>
          <tr align="center">
            <td>
              <a href="./shop_regist.php?shop_id=<?php echo $shop_id;?>"><img src="/app_management/images/btn_02.gif" alt="編集" /></a>　
              <a href="#" onClick='fnChangeSel("<?php echo $shop_id;?>");'><img src="/app_management/images/btn_03.gif" alt="削除" /></a>
            </td>
            <td>
							<?php echo $shop_id;?><br />
							<?php echo $shop_login_id;?>
            </td>
            <td>
							<?php
              if($img_1!="")
              {
              	echo "<img src='/".global_shop_dir.$shop_id."/".$img_1."' width=80 border=0>";
              }
              else
              {
              	echo "<img src='/img/common/noimg.jpg' width=80 border=0>";
              }
              ?>
            </td>
            <td><?php echo $shop_name;?></td>
            <td><?php echo $shop_percent;?></td>
            <td>
							<?php
              if($flag_open==1)
              {
                $open_check = "公開";
              }
              else
              {
                $open_check = "非公開";
              }
              echo $open_check;
              ?>
            </td>
            <td><?php echo $regi_date."<br />".$up_date;?></td>
          </tr>
<?php
  }
?>
        </table>

        <div style="text-align:center;">
          <?php $common_connect -> Fn_paging_10($view_count, $all_count); ?>
        </div><!-- //center-->
      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>