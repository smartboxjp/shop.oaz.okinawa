<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップ管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

<script language="javascript"> 
	function fnChangeSel(i) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './shop_del.php?shop_id='+i;
		} 
	}
	
	function fnImgDel(i, j, k) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './img_one_del.php?shop_id='+i+'&img='+j+'&img_name='+k;
		} 
	}
	
	var gWinGMap;
	function OpenWinGoogleMap(aVal) {
		if(aVal == "shop")
		{
			var nLat	= $("#google_map_1").val();
			var nLon	= $("#google_map_2").val();
		}
		else
		{
			var nLat	= $("#google_parking_1").val();
			var nLon	= $("#google_parking_2").val();
		}

		var nWidth;
		var nHeight;
		
		nWidth	= 580;
		nHeight	= 700;
	
		var nTop	= ( window.screen.height / 2 ) - ( nHeight / 2 );
		var nLeft	= ( window.screen.width  / 2 ) - ( nWidth  / 2 );
	
		var sUrl = "";
		sUrl    = "/app_management/include/google_map.php?google_map_1="+nLat+"&google_map_2="+nLon+"&kubun="+aVal;
	
		var sOption = "top=" + nTop + ",left=" + nLeft + ",width=" + nWidth + ",height=" + nHeight + ",resizable=no";
		
		gWinGMap = window.open( sUrl, "gmap", sOption );
		gWinGMap.focus();
	
		return false;
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input_id("shop_id");
			err_check_count += check_input_id("shop_login_id");
			err_check_count += check_input_id("shop_login_pw");
			err_check_count += check_input("shop_name");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		function check_input_id($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if(checkID($('#'+$str).val())==false)
			{
				err ="<br /><span style='color:#F00'>英数半角で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			else if($('#'+$str).val().length<4)
			{
				err ="<br /><span style='color:#F00'>4文字以上で入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
		//英数半角
		function checkID(value){
			if(value.match(/[^0-9a-z]+/) == null){
				return true;
			}else{
				return false;
			}
		} 

	});
//-->
</script>
</head>

<body>
<?
	
	//管理者チェック
	if ($_SESSION['admin_id']=="" && $_SESSION['shop_id']=="")
	{
		$common_connect -> Fn_javascript_move("管理者専用ページです", "/app_management/");
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	//管理者ログインした場合GETの値でセッション渡す
	if ($_SESSION['admin_id']!="")
	{
		$shop_id = $_GET['shop_id'];
		$_SESSION['shop_id'] = $shop_id;
	}
	//サロンログインした場合セッションの値渡す
	else
	{
		$shop_id = $_SESSION['shop_id'];
	}
		
	if($shop_id!="")
	{
		$arr_db_field = array("shop_id", "shop_login_id", "shop_login_pw", "shop_name", "shop_kana", "shop_opentime", "shop_holliday");
		$arr_db_field = array_merge($arr_db_field, array("shop_percent", "shop_comment", "shop_email", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("tel", "shop_company", "shop_postnum", "shop_address", "shop_tantou"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
		$sql = "SELECT shop_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_shop where shop_id='".$shop_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<!--顧客データ登録メニュー-->
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_shop.php"; ?>
		<!--顧客データ登録メニュー-->
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">
				
				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>ショップ管理</p>
					</div>
				</div>
        <br />
        <div><a href="./shop_regist.php">ショップ新規登録</a></div>
        
        <section id="main" class="clearfix">
        <form action="./shop_save.php" method="POST" name="form_regist" id="form_regist" enctype="multipart/form-data">
				<?php $var = "check_up";?>
        <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<? echo $_GET["shop_id"];?>" />
        
        <table cellpadding="5" cellspacing="1" class="common_table input_table">
          <tr>
            <th>ショップID</th>
            <td>
            	<?php $var = "shop_id";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="<? if($$var!=""){ echo "hidden";} else { echo "text";}?>" value="<?php echo $$var;?>" />
              <?
								if($$var!="")
								{
									echo $$var."<p style=\"color:red\">変更不可</p>";
								}
								else
								{
									echo "<p>4文字以上の英語小文字、数字のみ入力可能</p>";
								}
							?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ログインID</th>
            <td>
            	<?php $var = "shop_login_id";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <p>4文字以上の英語小文字、数字のみ入力可能</p>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ログインPW</th>
            <td>
            	<?php $var = "shop_login_pw";?>
              <?
              	if($$var == "") { $$var = $common_connect->Fn_random_password(10);}
							?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <p>4文字以上の英語小文字、数字のみ入力可能</p>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ショップ名</th>
            <td>
            	<?php $var = "shop_name";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ショップカナ名</th>
            <td>
            	<?php $var = "shop_kana";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>コミッション</th>
            <td>
            	<?php $var = "shop_percent";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" class="w100" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>コメント</th>
            <td>
            	<?php $var = "shop_comment";?>
              <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" style="width:100%; height:50px;"><?php echo $$var;?></textarea>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ショップメール</th>
            <td>
            	<?php $var = "shop_email";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
              <?php if($$var!="") { ?>
              <br />
              <a href="shop_id_send.php?shop_id=<? echo $shop_id;?>" target="_blank"><?php echo $$var;?>のメールにログイン情報を送る</a>
              <? } ?>
            </td>
          </tr>
          <tr>
            <th>ロゴ</th>
            <td>
                <? 
                    $var = "img_1";
                ?>
                    <input type="file" name="<?=$var;?>" size="50"> 
                    <input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>"><br>
                <?
                    if ($$var != "")
                    {
                        echo "<a href='".global_no_ssl."/".global_shop_dir.$shop_id."/".$$var."' target='_blank'>".global_no_ssl."/".global_shop_dir.$shop_id."/".$$var."</a><br />";
                        echo "<a href='/".global_shop_dir.$shop_id."/".$$var."' target='_blank'><img src='/".global_shop_dir.$shop_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
                        echo "<br /><a href=\"#\" onClick='fnImgDel(\"".$shop_id."\", \"".$$var."\", \"".$var."\");'>上のファイル削除</a>";
                    }
                ?>
            </td>
          </tr>
          <tr>
          <tr>
            <th>電話番号</th>
            <td>
							<?php $var = "tel";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>請求先（会社名）</th>
            <td>
							<?php $var = "shop_company";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>担当者</th>
            <td>
							<?php $var = "shop_tantou";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>郵便番号</th>
            <td>
							<?php $var = "shop_postnum";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>住所</th>
            <td>
							<?php $var = "shop_address";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>営業時間</th>
            <td>
							<?php $var = "shop_opentime";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>定休日</th>
            <td>
							<?php $var = "shop_holliday";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr <? if($_SESSION["admin_id"]=="") { echo " style=\"display:none;\" ";}?>>
            <th>ステータス</th>
            <td>
            	<?php $var = "flag_open";?>
              <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="1" <? if($flag_open==1){echo "checked" ;}?>> 公開　</label>　
              <label><input type="radio" name="<?php echo $var;?>" id="<?php echo $var;?>" value="0" <? if($flag_open!=1){echo "checked" ;}?>> 非公開</label>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button input_table_confirm">
          <tr>
            <td>
              <input type="submit" value="この内容で作成" id="form_confirm">
            </td>
          </tr>
        </table>
        </form>
        </section>
        <br />


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>