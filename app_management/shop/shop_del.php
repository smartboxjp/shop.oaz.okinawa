<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップ登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	//管理者チェック
	$common_connect -> Fn_admin_check();
		
	$shop_id = $_GET['shop_id'];

	//DBへ保存
	if ($shop_id != "")
	{
		if(file_exists($_SERVER['DOCUMENT_ROOT']."/".global_shop_dir.$shop_id))
		{
			$common_connect -> Fn_deldir($_SERVER['DOCUMENT_ROOT']."/".global_shop_dir.$shop_id);
		}

		//商品情報削除
		$dbup = "delete from app_shop where shop_id = '$shop_id'";
		$db_result = $common_dao->db_update($dbup);
		
	}
	
	$common_connect -> Fn_javascript_move("ショップを削除しました。", "shop_list.php#search_list");

?>
</body>
</html>