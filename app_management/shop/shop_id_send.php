<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップ管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($shop_id != $_SESSION['shop_id'])
	{
		$common_connect -> Fn_javascript_back("ショップ情報は複数編集できません。");
	}
		
	if($shop_id!="")
	{
		$arr_db_field = array("shop_id", "shop_login_id", "shop_login_pw", "shop_name", "shop_kana", "shop_opentime", "shop_holliday");
		$arr_db_field = array_merge($arr_db_field, array("shop_percent", "shop_comment", "shop_email", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("tel", "shop_address"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
		$sql = "SELECT shop_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_shop where shop_id='".$shop_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<!--顧客データ登録メニュー-->
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_shop.php"; ?>
		<!--顧客データ登録メニュー-->
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">
				
				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p><?php echo $$var;?>のメールにログイン情報を送る</p>
					</div>
				</div>
        <br />
        
        <section id="main" class="clearfix">
        <form action="./shop_id_send_save.php" method="POST" name="form_regist" id="form_regist">
				<?php $var = "check_up";?>
        <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<? echo $_GET["shop_id"];?>" />
        
        <table cellpadding="5" cellspacing="1" class="common_table input_table">
          <tr>
            <th>タイトル</th>
            <td>
            	<?php $var = "subject";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="【<?php echo global_service_name;?>】<?php echo $shop_name;?>の情報" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>ショップメール</th>
            <td>
            	<?php $var = "shop_email";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>コメント</th>
            <td>
            	<?php $var = "body";?>
              <textarea name="<?php echo $var;?>" id="<?php echo $var;?>" style="width:100%; height:250px;">

■管理ページのURL
http://shop.okinawahans.com/app_management/

ショップログインID： <? echo $shop_login_id?>

ショップログインPW： <? echo $shop_login_pw?>

              
              </textarea>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button input_table_confirm">
          <tr>
            <td>
              <input type="submit" value="この内容でメールを送信する" id="form_confirm">
            </td>
          </tr>
        </table>
        </form>
        </section>
        <br />


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>