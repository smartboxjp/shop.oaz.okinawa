<?php
	$global_path = $_SERVER['DOCUMENT_ROOT']."/";

	require_once $global_path."app_include/connect.php";
	require_once $global_path."app_include/CommonEmail.php";

	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_email = new CommonEmail(); //メール関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>メルマガ送信</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	//ショップチェック
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	
	if($shop_id != $_SESSION['shop_id'])
	{
		$common_connect -> Fn_javascript_back("ショップ情報は複数編集できません。");
	}


	$common_email-> Fn_send_utf($shop_email."<".$shop_email.">",$subject,$body,$global_mail_from,$global_send_mail);
?>
<? echo $shop_email;?>へ送信完了しました。
<br /><a href="#" onClick="history.back();">戻る</a>
</body>
</html>