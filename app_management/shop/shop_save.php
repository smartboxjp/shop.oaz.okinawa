<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ショップ登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	if ($_SESSION['admin_id']=="" && $_SESSION['shop_id']=="")
	{
		$common_connect -> Fn_javascript_move("管理者専用ページです", "/shop/");
	}
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
		//echo $key.":".$value."<br />";
	}	
	
	if($shop_name == "")
	{
		$common_connect -> Fn_javascript_back("ショップ名を入力してください。");
	}
	
	if($check_up=="")
	{
		$sql = "select shop_login_id from app_shop where shop_login_id ='$shop_login_id'";
	
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			if($shop_login_id == $db_result[0]["shop_login_id"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているログインIDです。");
			}
		}
		
		$sql = "select shop_id from app_shop where shop_id ='$shop_id'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			if($shop_id == $db_result[0]["shop_id"])
			{
				$common_connect -> Fn_javascript_back("既に登録されているショップIDです。");
			}
		}
	}
	
	$datetime = date("Y/m/d H:i:s");

	//array
	$arr_db_field = array("shop_id", "shop_login_id", "shop_login_pw", "shop_name", "shop_kana", "shop_opentime", "shop_holliday");
	$arr_db_field = array_merge($arr_db_field, array("shop_percent", "shop_comment", "shop_email", "img_1"));
	$arr_db_field = array_merge($arr_db_field, array("tel", "shop_company", "shop_postnum", "shop_address", "shop_tantou"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open"));
	

	//基本情報
	if($check_up=="")
	{
		$db_insert = "insert into app_shop ( ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update app_shop set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where shop_id='".$shop_id."'";
	}
	$db_result = $common_dao->db_update($db_insert);
	
	//Folder生成
	$save_dir = $global_path.global_shop_dir.$shop_id."/";
	
	//Folder生成
	$common_image -> create_folder ($save_dir);
	
	$new_end_name="_1";
	$fname_new_name[1] = $common_image -> img_save("img_1", $common_connect, $save_dir, $new_end_name, $shop_id, $text_img_1, "500");
	
	$dbup = "update app_shop set ";
	for($loop = 1 ; $loop<2 ; $loop++)
	{
		$dbup .= " img_".$loop."='".$fname_new_name[$loop]."',";
	}
	$dbup .= " up_date='$datetime' where shop_id='".$shop_id."'";
	
	$db_result = $common_dao->db_update($dbup);

	$common_connect-> Fn_javascript_move("ショップ登録・修正しました", "shop_regist.php?shop_id=".$shop_id);
?>
</body>
</html>