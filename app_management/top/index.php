<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta charset="UTF-8">
<title><?=global_service_name;?> - 管理システム</title> 
</head>

<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/top/css/top.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

<!-- ↓↓AutoHeight↓↓ -->
<script type="text/javascript" src="/app_management/js/jqueryAutoHeight.js"></script>
<script type="text/javascript">
$(function(){
	$('.autoheight').autoHeight({
		height : 'height',
		reset : 'reset'
	});
});
</script>
<!-- ↑↑AutoHeight↑↑ -->

</head>

<body>

<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
?>
<div id="conteinar">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<!--メインコンテンツ-->
<div id="main_contents">
	<div class="pub_title01">
		<div class="pub_title01_inner">
			<p>管理メニュー</p>
		</div>
	</div>
	
	<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_shop.php"; ?>
    
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_customer.php"; ?>
    
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_uriage.php"; ?>

	</div>

</div>
<!--メインコンテンツ-->

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

</div>

</body>
</html>