<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>パトナー売上管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_admin_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}

	if($s_yyyymm == "")
	{
		$s_yyyymm = date("Ym");
	}
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">

	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_uriage.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->

	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">

				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>[パトナー]売上</p>
					</div>
				</div>
        <br />
      
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>登録年月</th>
            <td>
<?
	$sql = "SELECT DATE_FORMAT(regi_date, '%Y%m') as yyyymm FROM app_shop_uriage " ;
	//$sql .= " where shop_id='".$shop_id."' ";
	$sql .= " group by DATE_FORMAT(regi_date, '%Y%m') ";
	$sql .= " order by DATE_FORMAT(regi_date, '%Y%m') desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_s_yyyymm[] = $db_result[$db_loop]["yyyymm"];
		}
	}
						?>
							<?php $var = "s_yyyymm";?>
              <select name="<? echo $var;?>" id="<? echo $var;?>">
              	<option value="">---</option>
              	<?
                foreach($arr_s_yyyymm as $key=>$value)
								{
								?>
									<option value="<? echo $value;?>" <? if($value==$$var) { echo " selected ";}?>><? echo substr($value, 0, 4)."年".substr($value, 4, 2)."月";?></option>
								<?
								}
								?>
              
              </select>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="検索する" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$where = "";
	
	//リスト表示
	$arr_db_field = array("company_id", "company_percent", "company_title", "company_comment");
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_company where 1 ".$where ;
	$sql .= " order by regi_date desc ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
      		<h1 style="text-align:center;"><? echo date("Y年m月", strtotime($s_yyyymm."01"));?></h1>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th>パートナー名</th>
            <th>件数</th>
            <th>売り上げ</th>
            <th>手数料</th>
            <th>利益</th>
          </tr>

<?php
	for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
		
		//売上
		$today_count  = 0;
		$today_sum  = 0;
		$sql_uriage = "SELECT count(shop_uriage_id) as today_count, SUM( price ) as today_sum, SUM( (price*(shop_percent*0.01))*(company_percent*0.01)  ) as customer_price_1 FROM app_shop_uriage ";
		$sql_uriage .= " where company_id='".$company_id."' ";
		$sql_uriage .= " and status  < 90 ";
		if($s_yyyymm!="")
		{
			$sql_uriage .= " and DATE_FORMAT(regi_date, '%Y%m')  = '".$s_yyyymm."' ";
		}

		$db_result_uriage = $common_dao->db_query($sql_uriage);
		if($db_result_uriage)
		{
			$uriage_count = $db_result_uriage[0]["today_count"];
			$uriage_sum = $db_result_uriage[0]["today_sum"];
			$customer_price_1 = $db_result_uriage[0]["customer_price_1"];
		}
		
		//キャンセル
		$today_count  = 0;
		$today_sum  = 0;
		$sql_uriage = "SELECT count(shop_uriage_id) as today_count, sum(price) as today_sum, SUM( (price*(shop_percent*0.01))*(company_percent*0.01)  ) as customer_price_1 FROM app_shop_uriage ";
		$sql_uriage .= " where company_id='".$company_id."' ";
		$sql_uriage .= " and status  > 90 ";
		if($s_yyyymm!="")
		{
			$sql_uriage .= " and DATE_FORMAT(regi_date, '%Y%m')  = '".$s_yyyymm."' ";
		}

		$db_result_uriage = $common_dao->db_query($sql_uriage);
		if($db_result_uriage)
		{
			$cancel_count = $db_result_uriage[0]["today_count"];
			$cancel_sum = $db_result_uriage[0]["today_sum"];
			$cancel_customer_price_1 = $db_result_uriage[0]["customer_price_1"];
		}
?>
          <tr align="center">
            <td>
              <a href="/app_management/company/company_list.php?company_id=<?php echo $company_id;?>"><?php echo $company_title;?></a>
            </td>
            <td>
              <a href="/app_management/shop_uriage/shop_uriage_list.php?s_company_id=<?php echo $company_id;?>&s_yyyymm=<?php echo $s_yyyymm;?>&s_status=0#form_search"><? echo number_format($uriage_count); $sum_uriage_count += $uriage_count;?>件</a>
              <? if($cancel_count!=0) { echo "<br />キャンセル：".number_format($cancel_count);} ?>
            </td>
            <td>
							<? echo number_format($uriage_sum); $sum_uriage_sum += $uriage_sum;?>円
              <? if($cancel_sum!=0) { echo "<br />キャンセル：".number_format($cancel_sum);} ?>
            </td>
            <td><? echo $company_percent;?>％</td>
            <td>
							<?
								$uriage_company_percent = $customer_price_1;
								echo number_format($uriage_company_percent);
								$sum_uriage_company_percent += $uriage_company_percent;
							?>円
            </td>
          </tr>
<?php
	}
?>
          <tr align="center">
            <td style="background-color: rgb(255, 180, 0);">
              合計
            </td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_count);?>件</td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_sum);?>円</td>
            <td style="background-color: rgb(255, 180, 0);"> --- </td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_company_percent);?>円</td>
          </tr>
        </table>

      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>