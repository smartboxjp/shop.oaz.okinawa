<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>売上管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->


<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1/themes/redmond/jquery-ui.css" >
<script type="text/javascript">
  $(function() {
    $( '.date_added' ).datepicker( {
        format: 'yyyy/mm/dd',
        language: 'ja',
        autoclose: true,
        clearBtn: true,
    } );
    //$( '.date_added' ).datepicker().datepicker( 'setDate', 'today' );

});
</script>
</head>

<body>
<?
    //ショップチェック
    $common_connect -> Fn_admin_check();
    
    foreach($_GET as $key => $value)
    { 
        $$key = $common_dao->db_string_escape($value);
    }


    if($yyyymmdd_from==""){$yyyymmdd_from = date("Y/m/01");}
    if($yyyymmdd_to==""){$yyyymmdd_to = date("Y/m/t");}

?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">

    <!--サイドナビエリア-->
    <div id="sidenav">
        <div class="mainnav clearfix">
        <? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_uriage.php"; ?>
        </div>
    </div>
    <!--サイドナビエリア-->

    <!--第二階層メインコンテンツ-->
    <div id="sec_maincontents">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" valign="top">

                <div class="pub_title01">
                    <div class="pub_title01_inner">
                        <p>[加盟店]売上</p>
                    </div>
                </div>
        <br />
      
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>表示年月日</th>
            <td>
              <?php
                $var = "yyyymmdd_from";
              ?>
              <input type="text" name="<? echo $var;?>" class="date_added w200" value="<? echo $$var;?>">
              から

              <?php
                $var = "yyyymmdd_to";
              ?>
              <input type="text" name="<? echo $var;?>" class="date_added w200" value="<? echo $$var;?>">
              まで
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
                <input type="submit" value="検索する" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
    
    $where = "";
    
    //リスト表示
    $arr_db_field = array("shop_id", "shop_login_id", "shop_name", "shop_kana", "shop_percent");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM app_shop where 1 ".$where ;
    $sql .= " order by regi_date desc ";
    
    $db_result = $common_dao->db_query($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
?>
      <div id="search_list">
        <h1 style="text-align:center;"><? echo date("Y年m月d日", strtotime($yyyymmdd_from));?> 〜 <? echo date("Y年m月d日", strtotime($yyyymmdd_to));?></h1>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th>加盟店</th>
            <th>請求書発行</th>
            <th>件数</th>
            <th>売り上げ</th>
            <th>売上平均単価</th>
            <th>手数料</th>
            <th>利益</th>
            <th>手数料平均単価</th>
          </tr>

<?php
    for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
        
        $today_count  = 0;
        $today_sum  = 0;
        $sql_uriage = "SELECT count(shop_uriage_id) as today_count, sum(price) as today_sum FROM app_shop_uriage ";
        $sql_uriage .= " where shop_id='".$shop_id."' ";
        $sql_uriage .= " and (DATE_FORMAT(regi_date, '%Y/%m/%d')>='".$yyyymmdd_from."' and DATE_FORMAT(regi_date, '%Y/%m/%d')<='".$yyyymmdd_to."') ";
        $sql_uriage .= " and status<90 ";

        $db_result_uriage = $common_dao->db_query($sql_uriage);
        if($db_result_uriage)
        {
            $uriage_count = $db_result_uriage[0]["today_count"];
            $uriage_sum = $db_result_uriage[0]["today_sum"];
        }
?>
          <tr align="center">
            <td>
              <a href="/app_management/shop_uriage/shop_uriage_list.php?s_shop_id=<?php echo $shop_id;?>&s_yyyymm=<?php echo date("Ym", strtotime($yyyymmdd_from))?>&yyyymmdd_from=<?php echo $yyyymmdd_from;?>&yyyymmdd_to=<?php echo $yyyymmdd_to;?>#form_search"><?php echo $shop_name;?></a>
            </td>
            <td>
                <? if($uriage_count!=0) { ?>
              <a href="shop_uriage_excel.php?s_shop_id=<? echo $shop_id;?>&s_yyyymm=<?php echo date("Ym", strtotime($yyyymmdd_from))?>&yyyymmdd_from=<?php echo $yyyymmdd_from;?>&yyyymmdd_to=<?php echo $yyyymmdd_to;?>"><? echo date("Y年m月", strtotime($yyyymmdd_from));?><br />請求書</a>
              <? } ?>
            </td>
            <td>
                <? echo number_format($uriage_count); $sum_uriage_count += $uriage_count;?>件
            </td>
            <td><? echo number_format($uriage_sum); $sum_uriage_sum += $uriage_sum;?>円</td>
            <td><? if($uriage_sum!=0) { echo number_format($uriage_sum/$uriage_count)."円";}?></td>
            <td><? echo $shop_percent;?>％</td>
            <td>
                <?
                    $uriage_shop_percent = $uriage_sum*($shop_percent/100);
                    echo number_format($uriage_shop_percent);
                    $sum_uriage_shop_percent += $uriage_shop_percent;
                ?>円
            </td>
            <td><? if($uriage_shop_percent!=0) { echo number_format($uriage_shop_percent/$uriage_count)."円";}?></td>
          </tr>
<?php
    }
?>
          <tr align="center">
            <td style="background-color: rgb(255, 180, 0);">
              合計
            </td>
            <td style="background-color: rgb(255, 180, 0);"></td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_count);?>件</td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_sum);?>円</td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_sum/$sum_uriage_count);?>円</td>
            <td style="background-color: rgb(255, 180, 0);"> --- </td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_shop_percent);?>円</td>
            <td style="background-color: rgb(255, 180, 0);"><? echo number_format($sum_uriage_shop_percent/$sum_uriage_count);?>円</td>
          </tr>
        </table>

      </div><!-- //search_list-->
<?
    } //$db_result
?>


        </td>
      </tr>
    </table>
    </div>
    <!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>