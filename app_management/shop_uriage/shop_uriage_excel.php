<?

	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	$s_shop_id = $_GET['s_shop_id'];
	$s_yyyymm = $_GET['s_yyyymm'];
	
	//mysql_query("SET NAMES utf8");
	$db->query("SET NAMES utf8");
	$title = "請求書";

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2012 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2012 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.8, 2012-10-12
 */

/** Error reporting */
//error_reporting(E_ALL);

ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Tokyo');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
	
	
	//ClassesをおいたパスからPHPExcel.phpをインクルードする
	require_once($_SERVER['DOCUMENT_ROOT']."/app_management/php_excel/Classes/PHPExcel.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/app_management/php_excel/Classes/PHPExcel/IOFactory.php");
	

	$view_date = date("YmdHis");
	$in_file = "shop_uriage.xlsx";

	
	//読み込むExcelのパス
	$excel_file = $_SERVER['DOCUMENT_ROOT']."/app_management/shop_uriage/".$in_file;

	//.xls形式を読むという設定でクラスと作る
	//.xlsx形式を読み込む場合は'Excel5'の部分を'Excel2007'に変える →$reader=PHPExcel_IOFactory::createReader('Excel2007');
	$reader=PHPExcel_IOFactory::createReader('Excel2007');
	
	//読み込むExcelのパスを指定する
	$book=$reader->load($excel_file);
	
	// シートの設定を行う
	$book->setActiveSheetIndex(1);
	$sheet = $book->getActiveSheet();

	// セルに値をセットする
	$view_yyyymmdd = date("Y", strtotime($view_date))."年".date("m", strtotime($view_date))."月".date("d", strtotime($view_date))."日";
	$book->getActiveSheet()->setCellValue('H8', $view_yyyymmdd);
	
	if($s_shop_id!="")
	{
		$arr_db_field = array("shop_id", "shop_login_id", "shop_login_pw", "shop_name", "shop_kana", "shop_opentime", "shop_holliday");
		$arr_db_field = array_merge($arr_db_field, array("shop_percent", "shop_comment", "shop_email", "img_1"));
		$arr_db_field = array_merge($arr_db_field, array("tel", "shop_company", "shop_postnum", "shop_address", "shop_tantou"));
		$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
		
		$sql = "SELECT shop_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_shop where shop_id='".$s_shop_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
		if($shop_company=="")
		{
			$shop_company = $shop_name;
		}
	}
	else
	{
		$common_connect -> Fn_javascript_back("IDがありません。");
	}
	
	
	$book->getActiveSheet()->setCellValue('A9', $shop_company."　御中");
	$book->getActiveSheet()->setCellValue('A13', $shop_company."　御中");
	$book->getActiveSheet()->setCellValue('A14', intval(substr($s_yyyymm, 4, 2))."月オキゾーンパス ラウンジコミッションの件で請求書をお届け致しますので、発行月の末日までに下記口座へお振込み頂きますようお願い申し上げます。");
	$book->getActiveSheet()->setCellValue('B18', "但し、".intval(substr($s_yyyymm, 4, 2))."月オキゾーンパス ラウンジコミッションとして");
	

	$today_count  = 0;
	$today_sum  = 0;
	$sql_uriage = "SELECT count(shop_uriage_id) as today_count, sum(price) as today_sum FROM app_shop_uriage ";
	$sql_uriage .= " where shop_id='".$s_shop_id."' ";
	$sql_uriage .= " and DATE_FORMAT(regi_date, '%Y%m')  = '".$s_yyyymm."' ";
	$sql_uriage .= " and status<90 ";

	$db_result_uriage = $common_dao->db_query($sql_uriage);
	if($db_result_uriage)
	{
		$uriage_count = $db_result_uriage[0]["today_count"];
		$uriage_sum = $db_result_uriage[0]["today_sum"];
	}

	//$book->getActiveSheet()->setCellValue('E16', $uriage_sum);


	$book->setActiveSheetIndex(0);
	$sheet = $book->getActiveSheet();
	
	
	$book->getActiveSheet()->setCellValue('A1', $shop_company." (".intval(substr($s_yyyymm, 4, 2))."月分)（".$shop_percent."%）");
	
		//リスト表示
	$arr_db_field = array("shop_uriage_id", "shop_id", "shop_name", "shop_percent", "customer_id", "customer_name", "customer_name_en", "price", "comment");
	$arr_db_field = array_merge($arr_db_field, array("company_id", "company_title", "company_percent"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "status"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_shop_uriage ";
	$sql .= " where shop_id='".$s_shop_id."' ";
	$sql .= " and DATE_FORMAT(regi_date, '%Y%m')  = '".$s_yyyymm."' ";
	$sql .= " and status<90 ";
	$sql .= " order by shop_uriage_id desc";

	$sum_uriage_shop_percent=0;
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);

		for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$uriage_shop_percent = $uriage_sum*($shop_percent/100);
			$sum_uriage_shop_percent += $uriage_shop_percent;
			
			$book->getActiveSheet()->setCellValue('A'.(7+$db_loop), $customer_id);
			$book->getActiveSheet()->setCellValue('B'.(7+$db_loop), $shop_uriage_id);
			$book->getActiveSheet()->setCellValue('C'.(7+$db_loop), $customer_name_en);
			$book->getActiveSheet()->setCellValue('D'.(7+$db_loop), $company_title);
			$book->getActiveSheet()->setCellValue('E'.(7+$db_loop), $price);
			$book->getActiveSheet()->setCellValue('F'.(7+$db_loop), round($price*($shop_percent/100)));
			$book->getActiveSheet()->setCellValue('G'.(7+$db_loop), substr($regi_date, 0, 10));
		}
	}
// Rename worksheet
//$book->getActiveSheet()->setTitle($title);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$book->setActiveSheetIndex(0);

$out_file = $shop_company."_".$view_date.".xlsx";

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$out_file.'"');
header('Cache-Control: max-age=0');

$book = PHPExcel_IOFactory::createWriter($book, 'Excel2007');
$book->save('php://output');
exit;
?>