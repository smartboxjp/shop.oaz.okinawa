<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	$common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>売上登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//管理者チェック
	$common_connect -> Fn_shop_check();
	$shop_id = $_SESSION['shop_id'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($shop_id == "")
	{
		$common_connect -> Fn_javascript_back("名前を入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	
	if($shop_id!="")
	{
		$sql = "SELECT  shop_name FROM app_shop where shop_id='".$shop_id."' ";
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$shop_name = $db_result[0]["shop_name"];
		}
	}
	
	//array
	$arr_db_field = array("shop_id", "shop_name", "customer_id", "customer_name", "customer_name_en", "price", "comment");
	$arr_db_field = array_merge($arr_db_field, array("status"));

	//基本情報
	if($shop_uriage_id=="")
	{
		$db_insert = "insert into app_shop_uriage ( ";
		$db_insert .= " shop_uriage_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime')";
	}
	else
	{
		$db_insert = "update app_shop_uriage set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " shop_uriage_id='".$shop_uriage_id."' ";
		$db_insert .= " where shop_uriage_id='".$shop_uriage_id."'";
	}

	$db_result = $common_dao->db_update($db_insert);

				
	if ($shop_uriage_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$shop_uriage_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	$db_result = $common_dao->db_update($dbup);
	
	$common_connect-> Fn_javascript_move("売上登録・修正しました", "shop_uriage_list.php?shop_uriage_id=".$shop_uriage_id);
?>
</body>
</html>