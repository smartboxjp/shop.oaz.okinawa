<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	//ログインする時セッション消す
//	session_start();
//	session_destroy();
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	$admin_id = $common_dao->db_string_escape($_POST['admin_id']);
	$admin_pw = $common_dao->db_string_escape($_POST['admin_pw']);
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>ログインチェック</title>
</head>

<body>
<?
	if ($admin_id == "" or $admin_pw == "") 
	{
	    $common_connect->Fn_javascript_back("IDとパスワードを確認ください。");
	}
	else
	{
		$sql = "select admin_id, admin_name from app_admin where admin_id ='$admin_id' and admin_pw='$admin_pw'";
		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_admin_id = $db_result[0]["admin_id"];
			$db_admin_name = $db_result[0]["admin_name"];
		}
		
		if ($db_admin_id == $admin_id)
		{
			//管理者の場合
			session_start();
			$_SESSION['admin_id']=$db_admin_id;
			$_SESSION['admin_name']=$db_admin_name;
			$common_connect->Fn_redirect(global_ssl."/app_management/top/");

		}
		else
		{
			$common_connect->Fn_javascript_back("IDとPWを確認してください。");
		}
	}
?>
</body>
</html>