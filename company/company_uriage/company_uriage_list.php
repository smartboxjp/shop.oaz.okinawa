<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>매출관리</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_company_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	$s_company_id= $_SESSION["company_id"];
		
	if($shop_uriage_id!="")
	{

		$arr_db_field = array("shop_id", "shop_name", "customer_id", "customer_name", "customer_name_en", "price", "comment");
		$arr_db_field = array_merge($arr_db_field, array("status", "regi_date"));
			
		$sql = "SELECT shop_uriage_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_shop_uriage where shop_uriage_id='".$shop_uriage_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	$arr_db_field = array("shop_id", "shop_name");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_shop ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_shop_name[$db_result[$db_loop]["shop_id"]] = $db_result[$db_loop]["shop_name"];
		}
	}
	
	$arr_db_field = array("company_id", "company_title");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_company ";
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_company_title[$db_result[$db_loop]["company_id"]] = $db_result[$db_loop]["company_title"];
		}
	}
	
	if($customer_id!="")
	{
		$sql = "SELECT customer_name, customer_name_en FROM app_customer where customer_id='".$customer_id."'";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$customer_name = $db_result[0]["customer_name"];
			$customer_name_en = $db_result[0]["customer_name_en"];
		}
	}
	
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/company/include/header_in.php"; ?>
<!--ヘッダーエリア-->


<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_company.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->

	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">

				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>매출관리</p>
					</div>
				</div>
        <br />
      
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>키워드</th>
            <td>
							<?php $var = "s_keyword";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
          <tr>
            <th>ID</th>
            <td>
							<?php $var = "s_customer_id";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
          <tr>
            <th>등록일</th>
            <td>
<?
	$sql = "SELECT DATE_FORMAT(regi_date, '%Y%m') as yyyymm FROM app_shop_uriage " ;
	//$sql .= " where shop_id='".$shop_id."' ";
	$sql .= " group by DATE_FORMAT(regi_date, '%Y%m') ";
	$sql .= " order by DATE_FORMAT(regi_date, '%Y%m') desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_s_yyyymm[] = $db_result[$db_loop]["yyyymm"];
		}
	}
						?>
							<?php $var = "s_yyyymm";?>
              <select name="<? echo $var;?>" id="<? echo $var;?>">
              	<option value="">---</option>
              	<?
                foreach($arr_s_yyyymm as $key=>$value)
								{
								?>
									<option value="<? echo $value;?>" <? if($value==$$var) { echo " selected ";}?>><? echo substr($value, 0, 4)."年".substr($value, 4, 2)."月";?></option>
								<?
								}
								?>
              
              </select>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="검색하기" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (customer_name like '%".$s_keyword."%' or customer_name_en like '%".$s_keyword."%') ";
	}
	
	if($s_shop_id != "")
	{
		$where .= " and shop_id='".$s_shop_id."' ";
	}
	
	if($s_company_id != "")
	{
		$where .= " and company_id='".$s_company_id."' ";
	}
	
	if($s_customer_id != "")
	{
		$where .= " and customer_id='".$s_customer_id."' ";
	}
	
	
	if($s_yyyymm != "")
	{
		$where .= " and DATE_FORMAT(regi_date, '%Y%m')='".$s_yyyymm."' ";
	}


	//合計
	$sql_count = "SELECT count(shop_uriage_id) as all_count FROM app_shop_uriage where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("shop_uriage_id", "shop_id", "shop_name", "shop_percent", "customer_id", "customer_name", "customer_name_en", "price", "comment");
	$arr_db_field = array_merge($arr_db_field, array("company_id", "company_title", "company_percent"));
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "status"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_shop_uriage where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by shop_uriage_id desc";
	}
	$sql .= " limit $offset,$view_count";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
        すべて：<?=$all_count;?>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th>ID</th>
            <th>
            고객명<br />
            영문명
            </th>
            <th>가맹점</th>
            <th>커미션</th>
            <th>금액</th>
            <th>상태</th>
            <th>등록일</th>
          </tr>
          <tr align="center">
            <td><a href="?order_name=shop_uriage_id&order=desc">▼</a>｜<a href="?order_name=shop_uriage_id">▲</a></td>
            <td><a href="?order_name=customer_name&order=desc">▼</a>｜<a href="?order_name=customer_name">▲</a></td>
            <td><a href="?order_name=shop_name&order=desc">▼</a>｜<a href="?order_name=shop_name">▲</a></td>
            <td><a href="?order_name=customer_name&order=desc">▼</a>｜<a href="?order_name=customer_name">▲</a></td>
            <td><a href="?order_name=price&order=desc">▼</a>｜<a href="?order_name=price">▲</a></td>
            <td><a href="?order_name=status&order=desc">▼</a>｜<a href="?order_name=status">▲</a></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
	for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
          <tr align="center">
            <td><?php echo $shop_uriage_id;?></td>
            <td>
							<?php echo $customer_name;?><br />
              <?php echo $customer_name_en;?>
            </td>
            <td>
              <?php echo $shop_name;?>
            </td>
            <td><?php if($status=="99") {echo "―";}?><? echo ($price*($shop_percent/100))*($company_percent/100);?>円
            </td>
            <td><?php echo number_format($price); if($status<"90") { $sum_price+=($price*($shop_percent/100))*($company_percent/100); $sum_count++;} else { $sum_cancel_price+=($price*($shop_percent/100))*($company_percent/100); $sum_cancel_count++;} ?>円</td>
            <td><span style="color:#F00"><?php if($status=="99") {echo "취소";}?></span></td>
            <td><?php echo $regi_date;?></td>
          </tr>
<?php
	}
?>
          <tr align="center">
          	<td></td>
          	<td style="background-color: rgb(255, 180, 0);" colspan="7">
            	매출금액：<?php echo number_format($sum_price);?>円（<?php echo number_format($sum_count);?>건）　
              취소：<?php echo number_format($sum_cancel_price);?>円（<?php echo number_format($sum_cancel_count);?>건）
            </td>
          </tr>
        </table>

        <div style="text-align:center;">
          <?php $common_connect -> Fn_paging($view_count, $all_count); ?>
        </div><!-- //center-->
      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>