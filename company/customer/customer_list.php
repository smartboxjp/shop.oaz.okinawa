<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連

    $arr_lounge_place[1] = "OAZ본점";
    $arr_lounge_place[2] = "OAZ국제거리점";
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>고객관리</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

<script language="javascript"> 
	function fnChangeSel(i, j) { 
		var result = confirm('削除しますか？'); 
		if(result){ 
			document.location.href = './customer_del.php?company_id='+i+'&customer_id='+j;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("customer_name");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
//-->
</script>
</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_company_check();
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	$company_id = $_SESSION['company_id'];
	
		
	if($customer_id!="")
	{

		$arr_db_field = array("customer_id", "customer_name", "company_id", "customer_name_en");
		$arr_db_field = array_merge($arr_db_field, array("cate_air_in_id", "lounge_place", "check_in", "check_out"));
		$arr_db_field = array_merge($arr_db_field, array("customer_tel", "customer_comment", "customer_email", "customer_kakao", "pay_money"));
		$arr_db_field = array_merge($arr_db_field, array("regi_date", "up_date"));
			
		$sql = "SELECT customer_id, ";
		foreach($arr_db_field as $val)
		{
			$sql .= $val.", ";
		}
		$sql .= " 1 FROM app_customer where customer_id='".$customer_id."'";
		$sql .= " and company_id='".$company_id."' ";
		
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[0][$val];
			}
		}
	}
	
	//カテゴリをArrayへ
	$arr_db_field = array("company_id", "company_title", "company_percent");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_company ";
	$sql .= " where company_id='".$company_id."' ";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[0][$val];
		}
	}
	
	$arr_db_field = array("cate_air_in_id", "cate_air_in_name");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_cate_air_in order by view_level";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_air_in_name[$db_result[$db_loop]["cate_air_in_id"]] = $db_result[$db_loop]["cate_air_in_name"];
		}
	}
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/company/include/header_in.php"; ?>
<!--ヘッダーエリア-->


<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_company.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">

				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>고객관리</p>
					</div>
				</div>
        <br />
        <div><a href="<?=$_SERVER["PHP_SELF"];?>">고객등록</a></div>
        
        <section id="main" class="clearfix">
        <form action="./customer_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
				<?php $var = "company_id";?>
        <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="<?php echo $$var;?>" />
        <table cellpadding="5" cellspacing="1" class="common_table input_table">
          <tr>
            <th>고객ID</th>
            <td>
            	<?php if($customer_id=="") {echo "자동생성";} else { echo $customer_id;}?>
            	<input name="customer_id" type="hidden" value="<?php echo $customer_id;?>" />
            </td>
          </tr>
          <tr>
            <th>회원사</th>
            <td>
							<?php $var = "company_id";?>
              <input name="customer_id" type="hidden" value="<?php echo $customer_id;?>" /><? echo $company_title;?>

            </td>
          </tr>
          <tr>
            <th>고객명</th>
            <td>
							<?php $var = "customer_name";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>고객영문명</th>
            <td>
							<?php $var = "customer_name_en";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>전화번호</th>
            <td>
							<?php $var = "customer_tel";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>비행기(IN)</th>
            <td>
							<?php $var = "cate_air_in_id";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_cate_air_in_name as $key => $value)
              {
              ?>
                <option<? if (($$var == $key) && ($$var != "")) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>카드수령장소</th>
            <td>
                <?php $var = "lounge_place";?>
              <select name="<?=$var;?>" id="<?=$var;?>">
                <option value="" selected>---</option>
              <? 
              foreach($arr_lounge_place as $key => $value)
              {
              ?>
                <option<? if ($$var == $key) { echo " selected";}?> value="<?=$key;?>"><?=$value;?> </option>
              <?
              }
              ?>
              </select>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>CHECK IN</th>
            <td>
							<?php $var = "check_in";?>
              <? if($$var == "0000-00-00") {$$var = "";}?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <br />例）<? echo date("Y-m-d");?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>CHECK OUT</th>
            <td>
							<?php $var = "check_out";?>
              <? if($$var == "0000-00-00") {$$var = "";}?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <br />例）<? echo date("Y-m-d");?>
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>Email</th>
            <td>
							<?php $var = "customer_email";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>KAKAO TALK ID</th>
            <td>
							<?php $var = "customer_kakao";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
          <tr>
            <th>비고</th>
            <td>
							<?php $var = "customer_comment";?>
              <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" value="<?php echo $$var;?>" />
              <label id="err_<?php echo $var;?>"></label>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button input_table_confirm">
          <tr>
            <td>
              <input type="submit" value=" 등 록 " id="form_confirm">
            </td>
          </tr>
        </table>
        </form>
        </section>
        <br />
      
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>키워드</th>
            <td>
							<?php $var = "s_keyword";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="검색하기" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (customer_name like '%".$s_keyword."%' or customer_name_en like '%".$s_keyword."%' or customer_tel like '%".$s_keyword."%') ";
	}
	
	$where .= " and company_id='".$company_id."' ";

	//合計
	$sql_count = "SELECT count(customer_id) as all_count FROM app_customer where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("customer_id", "customer_name", "company_id", "customer_name_en");
	$arr_db_field = array_merge($arr_db_field, array("cate_air_in_id", "check_in", "check_out"));
	$arr_db_field = array_merge($arr_db_field, array("customer_tel", "customer_comment", "customer_email", "customer_kakao", "pay_money"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_customer where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by up_date desc";
	}
	$sql .= " limit $offset,$view_count";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
        등록수 : <?=$all_count;?>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th width="70">편집</th>
            <th>고객ID</th>
            <th>고객명</th>
            <th>영문명</th>
            <th>check in</th>
            <th>飛行機<br >in</th>
            <th>금액</th>
            <th>정산금액</th>
            <th>등록일<br />수정일</th>
          </tr>
          <tr align="center">
            <td>sort</td>
            <td><a href="?order_name=customer_id&order=desc">▼</a>｜<a href="?order_name=customer_id">▲</a></td>
            <td><a href="?order_name=customer_name&order=desc">▼</a>｜<a href="?order_name=customer_name">▲</a></td>
            <td><a href="?order_name=customer_name_en&order=desc">▼</a>｜<a href="?order_name=customer_name_en">▲</a></td>
            <td><a href="?order_name=check_in&order=desc">▼</a>｜<a href="?order_name=check_in">▲</a></td>
            <td><a href="?order_name=cate_air_in_id&order=desc">▼</a>｜<a href="?order_name=cate_air_in_id">▲</a></td>
            <td></td>
            <td></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
	$all_pay_count = 0;
	$all_sum_price = 0;
	
	for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
          <tr align="center">
            <td>
              <a href="?customer_id=<?php echo $customer_id;?>">편집</a>　
              <a href="#" onClick='fnChangeSel("<?php echo $company_id;?>", "<?php echo $customer_id;?>");'>삭제</a>
            </td>
            <td><?php echo $customer_id;?></td>
            <td><?php echo $customer_name;?></td>
            <td><?php echo $customer_name_en;?></td>
            <td><?php if($check_in!="0000-00-00"){ echo $check_in; }?></td>
            <td><?php echo $arr_cate_air_in_name[$cate_air_in_id];?></td>

            <td>
							<?php 
							$sql_customer = "SELECT COUNT( price ) as customer_count, SUM( price ) as customer_price, SUM( (price*(shop_percent*0.01))*(company_percent*0.01)  ) as customer_price_1 FROM  app_shop_uriage WHERE company_id ='".$company_id."' and customer_id ='".$customer_id."' and status!=99";
							$db_result_customer = $common_dao->db_query($sql_customer);
							if($db_result_customer)
							{
								echo $db_result_customer[0]["customer_count"]."件<br />";
								echo number_format($db_result_customer[0]["customer_price"])."円";
							}
							?>
            </td>
            <td>
							<?php
								$all_pay_count++;
								//$all_sum_price += $db_result_customer[0]["customer_price"]*($company_percent*0.01);
								//echo number_format($db_result_customer[0]["customer_price"]*($company_percent*0.01));
								$all_sum_price += $db_result_customer[0]["customer_price_1"];
								echo number_format($db_result_customer[0]["customer_price_1"]);
              ?>円
            </td>
            <td><?php echo $regi_date."<br />".$up_date;?></td>
          </tr>
<?php
	}
?>
          <tr align="center">
          	<td></td>
          	<td style="background-color: rgb(255, 180, 0);" colspan="8">
            커미션 : <? echo $company_percent;?>%
            	정산금액:<?php echo number_format($all_sum_price);?>円（<?php echo number_format($all_pay_count);?>件）　
            </td>
          </tr>
        </table>

        <div style="text-align:center;">
          <?php $common_connect -> Fn_paging($view_count, $all_count); ?>
        </div><!-- //center-->
      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>