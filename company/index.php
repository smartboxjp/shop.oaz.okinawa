<?
	if(!$_SERVER['HTTPS'])
	{
		//header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	}
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
		
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="googlebot" content="noindex">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title><? echo global_service_name;?>관리시스템</title>

<!--↓↓共通スタイル↓↓-->
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/css/layout.css" media="all" />

<!--jQuery-->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!--jQuery-->

<!--accordion menu-->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!--accordion menu-->
<!--↑↑共通スタイル↑↑-->

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("company_login_id");
			err_check_count += check_input("company_login_pw");
			
			if(err_check_count!=0)
			{
				alert("정확히 입력해 주세요");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>정확히 입력해 주세요</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

	});
	
//-->
</script>
</head>

<body>
<div id="container">

<!--ヘッダーエリア-->
<div id="header">
  <div class="header_left">
    <h1><? echo global_service_name;?>관리시스템</h1>
  </div>
</div>
<!--ヘッダーエリア-->

<!--パンくずエリア-->
<div id="searchnav">
  <p class="px14 white bold">[ 로그인 ]</p>
</div>
<!--パンくずエリア-->

<!--コンテンツエリア-->
<div id="main_contents">
	<div class="login_box_outer">
    <form action="/company/login_check.php" name="form_regist" id="form_regist" method="post">
    <div class="login_box01">
      <p class="id01">로그인 ID</p>
		<? $var = "company_login_id";?>
        <input type="text" name="<?=$var;?>" id="<?=$var;?>" />
        <label id="err_<?=$var;?>"></label>
      <p class="pw01">패스워드</p>
		<? $var = "company_login_pw";?>
        <input type="password" name="<?=$var;?>" id="<?=$var;?>" />
        <label id="err_<?=$var;?>"></label>
    </div>
    
    <div class="login_box02">
			<? $var = "customer_id";?>
			<input type="hidden" name="<?=$var;?>" id="<?=$var;?>" value="<?=$$var;?>" />
			
      <input type="submit" id="form_confirm" name="form_confirm" value="로그인" />
    </div>
    </form>
  </div>
</div>
<!--コンテンツエリア-->

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer.php"; ?>
<!--フッターエリア-->

</div>
</body>
</html>