<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<title>관리시스템</title>
</head>
<body>
<?
	
	$customer_id = trim($_POST['customer_id']);
	$company_login_id = trim($_POST['company_login_id']);
	$company_login_pw = trim($_POST['company_login_pw']);

	if ($company_login_id == "" or $company_login_pw == "") 
	{
	    $common_connect -> Fn_javascript_back("IDとパスワードを確認ください。");
	}
	else
	{
		$sql = "select company_id,  company_title  from app_company where company_login_id ='$company_login_id' and company_login_pw='$company_login_pw'";

		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_company_id = $db_result[0]["company_id"];
			$db_company_title = $db_result[0]["company_title"];
		}
		
		if (trim($db_company_id) != "")
		{
			//管理者の場合
			session_start();
			$_SESSION['company_id']=$db_company_id;
			$_SESSION['company_title']=$db_company_title;
			
			
			$url = "/company/customer/customer_list.php";
			
			$common_connect -> Fn_redirect($url);
		}
		else
		{
			$common_connect -> Fn_javascript_back("IDとPWを確認してください。");
		}

	}
?>
</body>
</html>