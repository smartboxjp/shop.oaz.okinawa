<?php
class CommonDao{

  protected $mysqli = null;

  //コンストラクタ
  function __construct(){
    $this->connect();
  }

  //デストラクタ
  function __destruct(){
    $this->disconnect();
  }

  //MySQLサーバへ接続
  protected function connect(){
    if(is_null($this->mysqli)){
      $this->mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
      $this->mysqli->query("SET NAMES utf8");
      if(mysqli_connect_errno()){
        die("MySQLサーバ接続に失敗しました<br> 理由：" . mysqli_connect_error());
      }
    }
  }

  //MySQLサーバと切断
  protected function disconnect(){
    is_null($this->mysqli) or $this->mysqli->close();
  }

  public function transaction_start(){
    is_null($this->mysqli) and $this->connect();
    if( !$this->mysqli->query("set autocommit = 0")){
      return "トランザクション開始に失敗しました(1)" . $this->mysqli->error . "\n";
    }else{
    	if(!$this->mysqli->query("begin")){
    		return "トランザクション開始に失敗しました(2)" . $this->mysqli->error . "\n";
    	}else{
    		return true;
    	}
    }
  }

  public function transaction_end(){
    if( !$this->mysqli->query("commit")){
      $this->mysqli->query("rollback");
      return "コミットに失敗しました" . $this->mysqli->error . "\n";
    }
    return true;
  }

  public function transaction_rollback(){
    $this->mysqli->query("rollback");
  }

  //DB Query
  public function db_query($sql){
    is_null($this->mysqli) and $this->connect();
    $result = $this->mysqli->query($sql);
    $db_result = array();
		
		if($result){
			while($row = $result->fetch_array(MYSQLI_ASSOC)){
				$db_result[] = $row;
			}
			$result->close();
		}else{
			$db_result = null;
		}
    return $db_result;
  }

  //DB Query with Transaction
  public function db_query_ts($sql){
    $result = $this->mysqli->query($sql);
    $db_result = array();

    if($result){
	    while($row = $result->fetch_array(MYSQLI_ASSOC)){
	      $db_result[] = $row;
	    }
	    $result->close();
    }
    return $db_result;
  }

  //DB Update
  public function db_update($sql){
    is_null($this->mysqli) and $this->connect();
    if( !$this->mysqli->query($sql)){
      return "更新に失敗しました" . $this->mysqli->error . "\n";
    }
    return true;
  }

  //DB Update with Transaction
  public function db_update_ts($sql){
    $result = $this->mysqli->query($sql);
    if( !$this->mysqli->query($sql)){
      return "更新に失敗しました" . $this->mysqli->error . "\n";
    }
    return true;
  }

  public function db_string_escape($string){
		$result = $this->mysqli->real_escape_string($string);
		return str_replace("\\r\\n", "\r\n", $result);
  }

}

?>
