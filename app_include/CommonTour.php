<?php

$arr_flag_open[1] = "공개";
$arr_flag_open[0] = "비공개";

$arr_tour_status[1] = "출발확정";
$arr_tour_status[0] = "예약가능";
$arr_tour_status[2] = "예약마감";

$arr_tour_reserve[1] = "예약하기";
$arr_tour_reserve[0] = "대기예약";
	
?>

				
<?php
	if($s_yyyymm=="")
	{
		$s_yyyymm = date("Ym");
	}
	$where = "";
	$where .= " and flag_open='1' ";
	$where .= " and YEAR(from_date)='".substr($s_yyyymm, 0, 4)."' ";
	$where .= " and MONTH(from_date)='".substr($s_yyyymm, 5, 2)."' ";
	$where .= " and from_date>='".date("Y-m-d")."' ";
	
	//リスト表示
	$arr_db_field = array("tour_id", "tour_title", "from_date", "tour_price");
	$arr_db_field = array_merge($arr_db_field, array("tour_person", "tour_status", "tour_reserve"));
	$arr_db_field = array_merge($arr_db_field, array("flag_open", "regi_date", "up_date"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM tour where 1 ".$where ;
	$sql .= " order by from_date desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
?>
			<link rel="stylesheet" type="text/css" href="/css/tour/list.css">
      <section class="genre_list">
        <h2>
        	투어예약
        </h2>
        <br />
<?
				$where_sub = "";
				$where_sub .= " and flag_open='1' ";
				$where_sub .= " and DATE_FORMAT(from_date, '%Y%m')>='".date("Ym", strtotime("0 month"))."' ";
				$where_sub .= " and from_date>='".date("Y-m-d")."' ";
				
				$sql_sub = "SELECT ";
				$sql_sub .= " DATE_FORMAT(from_date, '%Y%m') as yyyymm " ;
				$sql_sub .= " FROM tour where 1 ".$where_sub ;
				$sql_sub .= " group by DATE_FORMAT(from_date, '%Y%m') ";
				$sql_sub .= " order by DATE_FORMAT(from_date, '%Y%m') ";
				$db_result_sub = $common_dao->db_query($sql_sub);
				if($db_result_sub)
				{
					for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
					{
						echo "　<a href='?s_yyyymm=".$db_result_sub[$db_loop_sub]["yyyymm"]."#tour' style='color:#0034FF; font-size:13px;'>".substr($db_result_sub[$db_loop_sub]["yyyymm"], 0, 4)."年".substr($db_result_sub[$db_loop_sub]["yyyymm"], 4, 2)."月"."</a>　";
					}
				}
?>

        <table class="tour" id="tour">
        	<tr class="tour_title">
						<td>
            	번호
          	</td>
						<td>
            	예약상태
          	</td>
						<td>
            	투어명
          	</td>
						<td>
            	투어진행일
          	</td>
						<td>
            	예약
          	</td>
						<td>
            	가격
          	</td>
						<td>
            	예약하기
          	</td>
					</tr>

<?
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			foreach($arr_db_field as $val)
			{
				$$val = $db_result[$db_loop][$val];
			}
			
			$reserve_all_count = 0;
			$sql_count = "SELECT sum(reserve_person) as reserve_all_count FROM reserve_tour where status!='취소' and tour_id='".$tour_id."'";
			
			$db_result_count = $common_dao->db_query($sql_count);
			if($db_result_count)
			{
				$reserve_all_count = $db_result_count[0]["reserve_all_count"];
			}
			if($reserve_all_count==""){ $reserve_all_count=0;}
?>
        	<tr class="tour_list">
						<td class="tour_num">
            	<? echo $tour_id;?>
          	</td>
						<td>
            	<span class="status_button_<? echo $tour_status;?>"><? echo $arr_tour_status[$tour_status];?></span>
          	</td>
						<td>
							<? echo $tour_title;?>
          	</td>
						<td class="tour_date">
            	<? echo substr($from_date, 0, 10)."<br />(".$common_connect->Fn_date_day($from_date).")".substr($from_date, 11, 5);?>
          	</td>
						<td>
            	<? echo $reserve_all_count."/".number_format($tour_person);?>
          	</td>
						<td>
            	<? echo number_format($tour_price);?>円
          	</td>
						<td>
            	<?
							if($tour_reserve==1 && ($tour_person>$reserve_all_count))
							{
							?>
            		<a href="/tour_reserve/tour_reserve.php?tour_id=<? echo $tour_id;?>"><span class="pre_button_<? echo $tour_reserve;?>"><? echo $arr_tour_reserve[$tour_reserve];?></span></a>
							<?
							}
							else
							{
								$tour_reserve=0;
							?>
            		<span class="pre_button_<? echo $tour_reserve;?>"><? echo $arr_tour_reserve[$tour_reserve];?></span>
							<?
							}
							?>
          	</td>
					</tr>
<?
		}
?>
        </table>
      </section>
<?
	}
?>