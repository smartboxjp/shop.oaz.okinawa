<?php

class CommonConnect{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	public function Fn_admin_check()
	{
		if(isset($_SESSION['admin_id'])!=true || $_SESSION['admin_id'] == '')
		{
			$this -> Fn_javascript_move("관리자 전용 페이지 입니다.", "/app_management/admin/");
		}
	}
	
	public function Fn_shop_check()
	{
		global $common_dao;
		
		if(isset($_COOKIE['cook_gameiten'])!=true || $_COOKIE['cook_gameiten'] == '')
		{
			$this -> Fn_javascript_move("管理者専用ページです。", "/shop/");
		}
		
		//ログイン情報が変わったら再ログイン必要
		$sql = "select shop_id, shop_name, img_1 from app_shop where login_cookie ='".$_COOKIE['cook_gameiten']."'";

		$db_result = $common_dao->db_query($sql);
		if(!$db_result)
		{
			$this -> Fn_javascript_move("管理者専用ページです。", "/shop/");
		}
	}
	
	public function Fn_company_check()
	{
		if(isset($_SESSION['company_id'])!=true || $_SESSION['company_id'] == '')
		{
			$this -> Fn_javascript_move("관리자 전용 페이지 입니다.", "/company/");
		}
	}
	

	/* すべて半角に変換 */
	public function Fn_shiftjis($str)
	{
		$str= mb_convert_kana($str,"rnask","UTF-8");
	
		return $str;
		/* すべて全角に変換 */
		//$str = mb_convert_kana($str,"RNASKV","EUC-JP");
	}

	//メールチェック
	public function Fn_valid_email($mail_address)
	{
		if (!preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$mail_address))
			return false;
		else
			return true;
	}
	
	//Javascriptのhistoryback
	public function Fn_javascript_back($message)
	{
		echo ("
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			alert('$message');
			history.back();
			//-->
			</SCRIPT>
		");
		exit;
	}
	
	//Javascriptのmove
	public function Fn_javascript_move($message1, $message2)
	{
		echo ("
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			alert('$message1');
			document.location.href = '$message2';
			//-->
			</SCRIPT>
		");
		exit;
	}

	//URL移動
	public function Fn_redirect($url)
	{
		echo ("<meta http-equiv='Refresh' content='0; URL=$url'>");
		exit;
	}
	
	//文字化け対策
	//input [real_escape_string] -> output[htmlspecialchars]
  public function str_htmlspecialchars($string){
  	return htmlspecialchars($string);
  }

	public function h($string){
		return htmlspecialchars($string);
	}

	//SQL injection対策
	public function Fn_filter($str) 
	{
		$str=htmlspecialchars($str); //特殊文字を HTML エンティティに変換 例）&→&amp;
		$str=strip_tags($str); //html tag delete
		$str=addslashes($str);
		$str=mysql_real_escape_string($str);//文字列の特殊文字をエスケープ
		return $str;
	}

	
	//時間変更
	public function Fn_date($str1)
	{
		return substr($str1,0,4)."년".substr($str1,5,2)."월".substr($str1,8,2)."일";
	}
	
	//時間変更
	public function Fn_date_day($str1)
	{
		switch(date("w", strtotime($str1))){ 
			case "0": 
			$view_day="일"; 
			break; 
			case "1": 
			$view_day="월"; 
			break; 
			case "2": 
			$view_day="화"; 
			break; 
			case "3": 
			$view_day="수"; 
			break; 
			case "4": 
			$view_day="목"; 
			break; 
			case "5": 
			$view_day="금"; 
			break; 
			case "6": 
			$view_day="토"; 
			break; 
		}
		return $view_day;
	}
	
	//paging
	public function Fn_paging($view_count, $all_count)
	{
      if ($view_count < $all_count)
      {
        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".trim($value);
          }
        }
  
        for ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
        {
          if ($i == $page)
          {
            echo "[ ".$i." ]";
          }
          else
          {
            echo "[ <a href=".$_SERVER["PHP_SELF"]."?page=".$i.$query."#form_search>".$i."</a> ]";
          }
        }
      }
			
	}
	
	
	//paging
	public function Fn_paging_10($view_count, $all_count)
	{
		

        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".trim($value);
          }
        }
		$page = $_GET["page"];
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"paging\">";
		echo "<ul>";

		If ($page!="1")
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."#search_list\">←이전</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page+1).$query."#search_list\">다음→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}
	
	//paging 一覧
	public function Fn_paging_10_list($view_count, $all_count, $query)
	{
		foreach($_GET as $key => $value){ 
			if($key!="page" && $key != "submit_button" && $key != "arr_genre" && $key != "arr_area_m")
			{
				$query .= "&".$key."=".trim($value);
			}
		}
		$page = $_GET["page"];
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"paging\">";
		echo "<ul>";

		If ($page!="1")
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."#search_list\">←이전</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page+1).$query."#search_list\">다음→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}

	//フォルダ全て削除
	public function Fn_deldir($dir)
	{
		$handle = opendir($dir);
		while (false!==($FolderOrFile = readdir($handle)))
		{
			if($FolderOrFile != "." && $FolderOrFile != "..") 
			{ 
		
				if(is_dir("$dir/$FolderOrFile")) 
				{ $this -> Fn_deldir("$dir/$FolderOrFile"); } // recursive
					else
				{ unlink("$dir/$FolderOrFile"); }
			} 
		}
		closedir($handle);
		if(rmdir($dir))
		{ $success = true; }
		return $success; 
		
	} 
	
	//フォルダ内古いファイル削除
	//$deadline = 24*60*60;  //削除期限（指定秒数以上経過で削除）
	//$save_dir = $global_path.global_temp_img."/";
	public function Fn_old_delete($path, $deadline)
	{
		
		$count = 0;
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if (is_file($path."/".$file)) {
					if((time() - filemtime($path."/".$file) > $deadline)){
						if(unlink($path."/".$file)){
							//echo("{$file}を削除しました。<br />\n");
							//$count += 1;
						}else{
							//echo("{$file}の削除に失敗しました。<br />\n");
						}
					}else{
						//echo "{$file}は削除しませんでした。<br />\n";
					}
				}else{
					//echo("{$file}はファイルではありません。<br />\n");
				}
			}
			closedir($handle);
		}
		
		//echo("{$count}ファイル削除しました。");
	}
	
	//文字制限
	public function Fn_shot_string($str, $len)
	{
		if(mb_strlen($str, "UTF-8")>=$len)
		{
			$return_str = mb_substr($str, 0, $len, "UTF-8")."...";
		}
		else 
		{
			$return_str = $str;
		}
		
		return $return_str;
	}
	
		
	//directory内ファイルコピー
	public function copyDirectory($imageDir, $destDir)
	{
		$handle=opendir($imageDir);   
		while($filename=readdir($handle))
		{       
			if(strcmp($filename,".")!=0	&& strcmp($filename,"..")!=0)
		 {
			 if(is_dir("$imageDir/$filename"))
			 {
				if(!empty($filename) && !file_exists("$destDir/$filename"))
				mkdir("$destDir/$filename");
				copyDirectory("$imageDir/$filename","$destDir/$filename");
			 }
		 else
			 {
				if(file_exists("$destDir/$filename"))
				unlink("$destDir/$filename");
				copy("$imageDir/$filename","$destDir/$filename");
			 }
		 }
		}     
	}
	
	//ランダム生成
	public function Fn_random_password($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyz1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}
	
	//ランダム
	public function Fn_random_data($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}

}


?>
