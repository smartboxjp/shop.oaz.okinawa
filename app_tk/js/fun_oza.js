	$(function() {
		$('#form_confirm_check').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#f3f3f3";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("userNmEng");
			err_check_count += check_input("userYear");
			err_check_count += check_input("userTel");

			if(err_check_count)
			{
				alert("입력한 내용을 다시한번 확인해주세요");
				return false;
			}
			else
			{
				loginGo();
				return false;
			}
		});
		
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#f3f3f3";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("userNm");
			err_check_count += check_input("userNmEng");
			err_check_count += check_input("userYear");
			err_check_count += check_input("userTel");
			err_check_count += check_input("userSex");
			
						
			
			if(err_check_count)
			{
				alert("입력한 내용을 다시한번 확인해주세요");
				return false;
			}
			else
			{
				joinGo();
				return false;
				//$('#form_confirm').submit();
				//return true;
			}
			
			
		});
		
		
		$('#form_confirm_add').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#f3f3f3";
			bgcolor_err = "#FFCCCC";
			background = "background-color";
			
			err_check_count += check_input("cate_air_in_id");
			err_check_count += check_input("goDay");
			err_check_count += check_input("comeDay");
			
			
			if(err_check_count)
			{
				alert("입력한 내용을 다시한번 확인해주세요");
				return false;
			}
			else
			{
				joinGoAdd();
				return false;
				//$('#form_confirm').submit();
				//return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			//$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				//err ="<br /><span style='color:#F00'>정확히 입력해주세요</span>";
				//$("#err_"+$str).html(err);
				$('#'+$str).focus();
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
	
	

// 발급
function loginGo(){
		//PARAM 전달
		var params = {
			"customer_name_en":$('#userNmEng').val(),
			"customer_years":$('#userYear').val(),
			"customer_tel":$('#userTel').val()
		};
		
		jQuery.post("/app_tk/user_login_proc.php",$.param(params) ,
		function(data) {
			//alert('임시 화면 처리입니다.');
			var loginVO  = $.trim(data).split(",");
			 
			var userNmEng = loginVO[0];
			var ozaId = loginVO[1];
			var ozaBrt = loginVO[2];
			var ozaTel = loginVO[3];

			if(userNmEng!="")
			{
				// 안드로이드 API 영문 패스카드 아이디 , 생년월일 , 연락처 를 저장한다.
				var rs = window.android.setPassCard(userNmEng,ozaId,ozaBrt,ozaTel);
				if(rs == 'Y') {
				
					$('#card_id_num').text(ozaId);
					$('#card_user_nm').text(userNmEng);
					$('#bg_card').hide();
					$('#bg_after_card').show();
					$('#buz_send').show();
				}
			}
			else
			{
				alert("등록정보를 다시한번 확인하여 주십시요.");
			}

		});
}


function shuttleGo(){
   if($('#ozaId').val() == '') {
      alert('접근이 잘못되었습니다.');
      return;
   }
	 location.href = '/app_tk/join_add.php?ozaNmEng='+$('#ozaNmEng').val()+'&ozaId='+$('#ozaId').val()+'&ozaBrt='+$('#ozaBrt').val()+'&ozaTel='+$('#ozaTel').val();
   //$('#form_shuttle').submit();
}


function joinGo(){

		//PARAM 전달
		var params = {
			"customer_name":$('#userNm').val(),
			"customer_name_en":$('#userNmEng').val(),
			"customer_sex":$('#userSex').val(),
			"customer_years":$('#userYear').val(),
			"customer_tel":$('#userTel').val(),
			
		};

		jQuery.post("/app_tk/user_join_proc.php",$.param(params) ,
		function(data) {
			//alert('임시 화면 처리입니다.');
			var loginVO  = data.split(",");
			 
			var userNmEng = loginVO[0];
			var ozaId = loginVO[1];
			var ozaBrt = loginVO[2];
			var ozaTel = loginVO[3];
			
			if(userNmEng!="")
			{
				// 안드로이드 API 영문 패스카드 아이디 , 생년월일 , 연락처 를 저장한다.
				var rs = window.android.setPassCard(userNmEng,ozaId,ozaBrt,ozaTel);
				location.href = '/app_tk/main.php';
			}
			else
			{
				alert("등록중 에러가 발생하였습니다.");
			}

		});
}


function joinGoAdd(){

		//PARAM 전달
		var params = {
			"customer_id":$('#customer_id').val(),
			"customer_name_en":$('#customer_name_en').val(),
			"customer_years":$('#customer_years').val(),
			"customer_tel":$('#customer_tel').val(),
			
			"check_in":$('#goDay').val(),
			"check_out":$('#comeDay').val(),
			
			"cate_air_in_id":$('#cate_air_in_id').val(),
			"customer_comment":$('#customer_comment').val(),
			"bus_num":$('#busNum').val()
		};

		jQuery.post("/app_tk/user_add_proc.php",$.param(params) ,
		function(data) {
			//alert('임시 화면 처리입니다.');
			var loginVO  = data.split(",");
			var userNmEng = loginVO[0];
			var ozaId = loginVO[1];
			var ozaBrt = loginVO[2];
			var ozaTel = loginVO[3];
			
			if(userNmEng!="")
			{
				// 안드로이드 API 영문 패스카드 아이디 , 생년월일 , 연락처 를 저장한다.
				var rs = window.android.setPassCard(userNmEng,ozaId,ozaBrt,ozaTel);
				location.href = '/app_tk/main.php';
			}
			else
			{
				alert("등록중 에러가 발생하였습니다.");
			}

		});
}


$(function(){

	$('#btnJoin').click(function(){
		location.href="/app_tk/join.php";
	});

	$('#btnLogin').click(function(){
		$('#loginBox').show();
	});
	


	$('#contents').css('padding-top',$('#oza_box').height()+'px');

	//아이디를 가져온다 , 로 구분 되어있다 영문이름 , ID , 생년 , Tel
	var loginRs = window.android.getPassCard();

	if(loginRs != "N"){
		//PARAM 전달
		 var  loginVO = loginRs.split(",");
		var params = {
	 	  "customer_name_en":loginVO[0],
			"customer_id":loginVO[1],
			"customer_years":loginVO[2],
			"customer_tel":loginVO[3]
		};
		

      jQuery.post("/app_tk/user_login_check_proc.php",$.param(params) ,
         function(data) {

         if($.trim(data) == 'Y'){

            $('#card_id_num').text(loginVO[1]);
            $('#card_user_nm').text(loginVO[0]);

            $('#bg_card').hide();
            $('#bg_after_card').show();
            $('#buz_send').show();

            $('#ozaNmEng').val(loginVO[0]);
            $('#ozaId').val(loginVO[1]);
            $('#ozaBrt').val(loginVO[2]);
            $('#ozaTel').val(loginVO[3]);
						
         }else if($.trim(data) == 'N'){
            alert('패스카드 회원정보와 같은 정보가 없습니다.');
            window.android.setPassCard('','','','');       // 데이터 삭제
         }else{
            alert('접속이 원활하지 않습니다.');
         }

      });
	}

});