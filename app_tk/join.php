<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連


	foreach($_GET as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
	
	$arr_db_field = array("cate_air_in_id", "cate_air_in_name");
	$sql = "SELECT  ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_cate_air_in order by view_level";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_cate_air_in_name[$db_result[$db_loop]["cate_air_in_id"]] = $db_result[$db_loop]["cate_air_in_name"];
		}
	}
	
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/Common_card.php";
	
?>
<!DOCTYPE html>
<html lang="ko" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>OKINAWA</title>
		<link rel="stylesheet" type="text/css" href="/app_tk/css/oza.css" />
		<script type="text/javascript" src="http://okinawa.tpackage.com/js/beacontour/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="/app_tk/js/fun_oza.js"></script>
    

		</head>
	<body style="background-color:#f3f3f3;">
		<div id="wrap">
			<div id="oza_join_box">
      
        <form action="./index_confirm.php#form_regist" method="POST" name="form_write" id="form_regist">
        <?php $var = "flag_open";?>
        <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="hidden" value="1" />
				<div class="rowsbx">
					<table class="table_basic" >
						<tr>
							<td class="row_title">
								<label for="userNm">이름</label>&nbsp;&nbsp;&nbsp;<img src="/app_tk/img/vailck.png" width="14" style="vertical-align:middle;"/>
							</td>
							<td class="row_cont">
              		<?php $var = "userNm";?>
								<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" class="inp_text" placeholder="예) 홍길동"/>
                <label id="err_<?php echo $var;?>"></label>
							</td>
						</tr>
					</table>
				</div>

				<div class="rowsbx">
					<table class="table_basic" >
						<tr>
							<td class="row_title">
                <label for="userNmEng">영문이름</label>&nbsp;&nbsp;&nbsp;<img src="/app_tk/img/vailck.png" width="14" style="vertical-align:middle;"/>
							</td>
							<td class="row_cont">
              		<?php $var = "userNmEng";?>
								<input name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$_COOKIE[$var];?>" type="text" class="inp_text" placeholder="예) HONG KIL DONG"/>
							</td>
						</tr>
					</table>
				</div>

				<div class="rowsbx">
					<table class="table_basic" >
						<tr>
							<td class="row_title">
								<label for="userSex">성별</label>&nbsp;&nbsp;&nbsp;<img src="/app_tk/img/vailck.png" width="14" style="vertical-align:middle;"/>
							</td>
							<td class="row_cont">
              		<?php $var = "userSex";?>
								<select name="<?php echo $var;?>" id="<?php echo $var;?>" class="inp_sel" >
									<option value="">--</option>
									<option value="1">남자</option>
									<option value="2">여자</option>
								</select>
							</td>
						</tr>
					</table>
				</div>

				<div class="rowsbx">
					<table class="table_basic" >
						<tr>
							<td class="row_title">
								<label for="userYear">생년</label>&nbsp;&nbsp;&nbsp;<img src="/app_tk/img/vailck.png" width="14" style="vertical-align:middle;"/>
							</td>
							<td class="row_cont">
              		<?php $var = "userYear";?>
								<select name="<?php echo $var;?>" id="<?php echo $var;?>" class="inp_sel" >
									<option value="">--</option>
                  <?
                  	for($loop=date("Y") ; $loop>1920 ; $loop--)
										{
									?>
									<option value="<? echo $loop;?>"><? echo $loop;?></option>
									<?
											
										}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>


				
				<div class="rowsbx">
					<table class="table_basic" >
						<tr>
							<td class="row_title">
								<label for="userTel">연락처</label>&nbsp;&nbsp;&nbsp;<img src="/app_tk/img/vailck.png" width="14" style="vertical-align:middle;"/>
							</td>
							<td class="row_cont">
              		<?php $var = "userTel";?>
								<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="tel" class="inp_text" placeholder="- 제외하고 입력해주세요"/>
							</td>
						</tr>
					</table>
				</div>

					

				<div style="text-align:center;padding-top:10px;">
					<input id="form_confirm" type="image" src="/app_tk/img/btn_send.png" style="width:72px" />&nbsp;
					<a href="/app_tk/main.php"><img src="/app_tk/img/btn_can.png" style="width:72px" /></a>
				</div>
        </form>
			</div>

		</div>
</body>
</html>