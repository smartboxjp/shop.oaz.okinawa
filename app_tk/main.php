<!DOCTYPE html>
<html lang="ko" >
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>OKINAWA</title>
		<link rel="stylesheet" type="text/css" href="/app_tk/css/oza.css" />
		<script type="text/javascript" src="http://okinawa.tpackage.com/js/beacontour/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="/app_tk/js/fun_oza.js"></script>
		</head>
	<body>
		<div id="wrap">
			<div id="oza_box">


				<div id="bg_card" >
					<img src="/app_tk/img/card_before.png" width="318"  />
					<div class="btn_join">
						<img src="/app_tk/img/btn_join.png" width="72" id="btnJoin"/>
					</div>
					<div class="btn_login">
						<img src="/app_tk/img/btn_login.png" width="72" id="btnLogin"/>
					</div>

					<div  id="loginBox">
							<div style="background-color:#ffffff;border-radius:5px;height:126px;width:300px;padding-top:5px;" >
              <form action="" method="POST" name="form_write" id="form_regist">
								<div class="rowsbx" >
									<table class="table_basic"  >
										<tr>
											<td class="row_title">
												<label for="userNmEng" style="padding-right:2px;">영문이름</label>
											</td>
											<td class="row_cont">
                      		<?php $var = "userNmEng";?>
												<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="text" class="inp_text"  placeholder="예) HONG KIL DONG"/>
											</td>
										</tr>
									</table>
								</div>
								
								<div class="rowsbx" >
									<table class="table_basic" >
										<tr>
											<td class="row_title">
												<label for="userYear" style="padding-right:2px;">생년</label>
											</td>
											<td class="row_cont">
                      		<?php $var = "userYear";?>
												<select name="<?php echo $var;?>" id="<?php echo $var;?>" class="inp_sel" >
													<option value="">--</option>
													<?
                            for($loop=date("Y") ; $loop>1920 ; $loop--)
                            {
                          ?>
                          <option value="<? echo $loop;?>"><? echo $loop;?></option>
                          <?
                              
                            }
                          ?>
												</select>
											</td>
										</tr>
									</table>
								</div>
								<div class="rowsbx" >
									<table class="table_basic" >
										<tr>
											<td class="row_title">
												<label for="userTel" style="padding-right:2px;">연락처</label>
											</td>
											<td class="row_cont">
                      		<?php $var = "userTel";?>
												<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="tel" class="inp_text" placeholder="끝 4자리만 입력해주세요" maxlength="4" />
											</td>
										</tr>
									</table>
								</div>
							</div>

              <div class="btn_login_go">
                <input id="form_confirm_check" type="image" src="/app_tk/img/btn_send.png" style="width:72px;" />&nbsp;
              </div>
              <div class="btn_can">
                <input type="image" src="/app_tk/img/btn_can.png" style="width:72px" onclick="$('#loginBox').hide();"  />
              </div>




					</div>
				</div>


				<!-- 카드 등록후 팝업-->
				<div id="bg_after_card" style="display:none;">
					<img src="/app_tk/img/card_after.png" width="318"  />
					<div class="card_id_num" id="card_id_num"></div>
					<div class="card_user_nm" id="card_user_nm"></div>
				</div>


			</div>



			<div id="contents" style="margin-bottom:7px;padding-top:227px;">
				<img src="/app_tk/img/contents.png" width="100%"/>
			</div>
		</div>

      <!-- 로그인 되고 출력 -->
      <div id="buz_send" style="position:fixed;bottom:15px;right:7px;z-index:8;display:none;">
         <img src="/app_tk/img/btn_shuttle.png" width="75" onclick="shuttleGo()" />
      </div>
      <form method="post" name="form_shuttle" id="form_shuttle" action="./join_add.php">
         <input type="hidden" name="ozaNmEng" id="ozaNmEng" value=""/>
         <input type="hidden" name="ozaId" id="ozaId" value=""/>
         <input type="hidden" name="ozaBrt" id="ozaBrt" value=""/>
         <input type="hidden" name="ozaTel" id="ozaTel" value=""/>
      </form>


</body>
</html>