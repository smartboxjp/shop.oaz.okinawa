<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録・編集</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php	
	//ショップチェック
	$common_connect -> Fn_shop_check($shop_name);
	$cook_gameiten = $_COOKIE['cook_gameiten'];
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($cook_gameiten == "" || $customer_id == "")
	{
		$common_connect -> Fn_javascript_back("名前を入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//ショップ情報
	$sql = "select shop_name, shop_id, shop_percent from app_shop where login_cookie='".$cook_gameiten."' ";  
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$shop_name = $db_result[0]["shop_name"];
		$shop_id = $db_result[0]["shop_id"];
		$shop_percent = $db_result[0]["shop_percent"];
	}

	//顧客 
	$sql = "select customer_name, customer_name_en, company_id from app_customer where customer_id='".$customer_id."' ";  
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$customer_name = $db_result[0]["customer_name"];
		$customer_name_en = $db_result[0]["customer_name_en"];
		$company_id = $db_result[0]["company_id"];
	}
	
	
	//旅行会社 
	$sql = "select company_title, company_percent from app_company where company_id='".$company_id."' ";  
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$company_title = $db_result[0]["company_title"];
		$company_percent = $db_result[0]["company_percent"];
	}
		
	//array
	$arr_db_field = array("shop_id", "shop_name", "customer_id", "customer_name", "customer_name_en");
	$arr_db_field = array_merge($arr_db_field, array("company_id", "company_title", "price", "comment"));
	$arr_db_field = array_merge($arr_db_field, array("company_percent", "shop_percent"));

	//基本情報
	if($shop_uriage_id=="")
	{
		$db_insert = "insert into app_shop_uriage ( ";
		$db_insert .= " shop_uriage_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$common_dao->db_string_escape($$val)."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update app_shop_uriage set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$common_dao->db_string_escape($$val)."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where shop_uriage_id='".$shop_uriage_id."'";
	}

	$db_result = $common_dao->db_update($db_insert);

				
	if ($shop_uriage_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$shop_uriage_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	
	
	$common_connect-> Fn_javascript_move("売上を登録しました", "/shop/shop_uriage/shop_uriage_list.php");
?>
</body>
</html>