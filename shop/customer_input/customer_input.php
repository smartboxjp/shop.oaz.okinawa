<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	if($_COOKIE['cook_gameiten'] != '')
	{
		$sql = "select shop_id, shop_name, img_1 from app_shop where login_cookie ='".$_COOKIE['cook_gameiten']."'";

		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$shop_id = $db_result[0]["shop_id"];
			$shop_name = $db_result[0]["shop_name"];
			$h1_title = $db_result[0]["shop_name"];
			$img_1 = $db_result[0]["img_1"];
		}
	}
	
	if($shop_id=="raikamu")
	{
		$view_price_person = "総人数";
	}
	else
	{
		$view_price_person = "総金額";
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title><? echo global_service_name;?></title>

<!--↓↓共通スタイル↓↓-->
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/css/layout.css" media="all" />

<!--jQuery-->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!--jQuery-->

<!--accordion menu-->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!--accordion menu-->
<!--↑↑共通スタイル↑↑-->

<link href="/app_jquery/keyboard/jquery.keypad.css" rel="stylesheet">
<style>
body > iframe { display: none; }
#inlineKeypad { width: 10em; }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/app_jquery/keyboard/jquery.plugin.js"></script>
<script src="/app_jquery/keyboard/jquery.keypad.js"></script>
<script>
$(function () {
	$('.defaultKeypad').keypad();
	/*
	$('#inlineKeypad').keypad({onClose: function() {
		alert($(this).val());
	}});
	*/
});
</script>

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("customer_id");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

	});
	
//-->
</script>
</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_shop_check();
	$cook_gameiten = $_COOKIE['cook_gameiten'];
?>
<div id="container" style="background-color:rgb(255, 245, 217);">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">

<?
/*
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_gameiten.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->
*/
?>
	
  <!--コンテンツエリア-->
  <div id="main_contents">
    <div class="login_box_outer">
      <form action="/shop/customer_input/customer_input_confirm.php" name="form_regist" id="form_regist" method="post">
      <div class="login_box01">
        <h1><? echo $shop_name;?></h1>
        <?
					if ($img_1 != "")
					{
						echo "<img src='/".global_shop_dir.$shop_id."/".$img_1."' width=270 border=0>";
					}
				?>
        <p class="id01">お客様ID</p>
        <? $var = "customer_id";?>
          <input type="number" pattern="\d*" name="<?=$var;?>" id="<?=$var;?>"/>
          <label id="err_<?=$var;?>"></label>
        <p class="id01"><? echo $view_price_person;?></p>
        <? $var = "price";?>
          <input type="number" pattern="\d*" name="<?=$var;?>" id="<?=$var;?>"/><? // class="defaultKeypad"?>
          <label id="err_<?=$var;?>"></label>
      </div>
      
      <div class="login_box03">
        <input type="submit" id="form_confirm" name="form_confirm" value="登録" style="width:80%; height:50px;" />
      </div>
      </form>
    </div>
  </div>
  <!--コンテンツエリア-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer.php"; ?>
<!--フッターエリア-->

</div>
</body>
</html>