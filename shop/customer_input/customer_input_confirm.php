<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
		
	$cook_gameiten = $_COOKIE['cook_gameiten'];
	
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($cook_gameiten == "" || $customer_id == "")
	{
		$common_connect -> Fn_javascript_back("名前を入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//ショップ情報
	
	$sql = "select shop_id, shop_name, img_1 from app_shop where login_cookie ='".$cook_gameiten."'";

	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$shop_id = $db_result[0]["shop_id"];
		$shop_name = $db_result[0]["shop_name"];
		$h1_title = $db_result[0]["shop_name"];
		$img_1 = $db_result[0]["img_1"];
	}
	
	//顧客 
	$sql = "select customer_name, customer_name_en from app_customer where customer_id='".$customer_id."' ";  
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$customer_name = $db_result[0]["customer_name"];
		$customer_name_en = $db_result[0]["customer_name_en"];
	}
	
	
	if($shop_id=="raikamu")
	{
		$view_price_person = "総人数";
		$view_yen_person = "人";
	}
	else
	{
		$view_price_person = "総金額";
		$view_yen_person = "円";
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title><? echo global_service_name;?></title>

<!--↓↓共通スタイル↓↓-->
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/css/layout.css" media="all" />

<!--jQuery-->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!--jQuery-->

<!--accordion menu-->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!--accordion menu-->
<!--↑↑共通スタイル↑↑-->

<link href="/app_jquery/keyboard/jquery.keypad.css" rel="stylesheet">
<style>
body > iframe { display: none; }
#inlineKeypad { width: 10em; }
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
</head>

<body>
<?
	if($customer_name=="")
	{
		$common_connect -> Fn_javascript_back("登録されてないお客様IDです。");
	}
?>
<div id="container" style="background-color:rgb(255, 245, 217);">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/searchnav.php"; ?>
<!--サーチナビエリア-->


<!--コンテンツエリア-->
<div id="main_contents">
	<div class="login_box_outer">
    <form action="/shop/customer_input/customer_input_save.php" name="form_regist" id="form_regist" method="post">
    <div class="login_box01">
      <h1><? echo $shop_name;?></h1>
      <br />
      <p class="id01">お客様</p>
			<? $var = "customer_id";?>
        <input type="hidden" name="<?=$var;?>" id="<?=$var;?>" value="<? echo $$var;?>"/>
				<h2 style="color:#06F"><? echo $customer_name;?> 様</h2>
        <label id="err_<?=$var;?>"></label>
      <p class="id01">英語名</p>
				<h2 style="color:#06F"><? echo $customer_name_en;?> 様</h2>
      <br />
      <p class="id01"><? echo $view_price_person;?></p>
			<? $var = "price";?>
        <input type="hidden" name="<?=$var;?>" id="<?=$var;?>" value="<? echo $$var;?>"/>
        <h2 style="color:#06F"><? echo number_format($$var);?><? echo $view_yen_person;?></h2>
        <label id="err_<?=$var;?>"></label>
    </div>
    
      <div class="login_box03">
        <input type="submit" id="form_confirm" name="form_confirm" value="登録" style="width:80%; height:50px;" />
      </div>
    </form>
  </div>
</div>
<!--コンテンツエリア-->

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer.php"; ?>
<!--フッターエリア-->

</div>
</body>
</html>