<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
		
	if($_COOKIE['cook_gameiten'] != '')
	{
		$sql = "select shop_id, shop_name from app_shop where login_cookie ='".$_COOKIE['cook_gameiten']."'";

		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$db_shop_id = $db_result[0]["shop_id"];
			$db_shop_name = $db_result[0]["shop_name"];
		}
		
		if($db_shop_id!="")
		{
			//session_start();
			$_SESSION['shop_id']=$db_shop_id;
			$_SESSION['shop_name']=$db_shop_name;
			$common_connect->Fn_redirect(global_ssl."/shop/customer_input/customer_input.php");
		}
	}
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="googlebot" content="noindex">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>OAZ加盟店システム</title>

<!--↓↓共通スタイル↓↓-->
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/css/layout.css" media="all" />

<!--jQuery-->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!--jQuery-->

<!--accordion menu-->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!--accordion menu-->
<!--↑↑共通スタイル↑↑-->

<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("shop_login_id");
			err_check_count += check_input("shop_login_pw");
			
			if(err_check_count!=0)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				//$('#form_confirm').submit();
				$('#form_confirm', "body").submit();
				return true;
			}
			
			
		});
				
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);

			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}

	});
	
//-->
</script>
</head>

<body>
<div id="container">

<!--ヘッダーエリア-->
<div id="header">
  <div class="header_left">
    <h1><? echo global_service_name;?>管理システム</h1>
  </div>
</div>
<!--ヘッダーエリア-->

<!--パンくずエリア-->
<div id="searchnav">
  <p class="px14 white bold">[ 管理画面ログインページ ]</p>
</div>
<!--パンくずエリア-->

<!--コンテンツエリア-->
<div id="main_contents">
	<div class="login_box_outer">
    <form action="/shop/login_check.php" name="form_regist" id="form_regist" method="post">
    <div class="login_box01">
      <p class="id01">ログインID</p>
		<? $var = "shop_login_id";?>
        <input type="text" name="<?=$var;?>" id="<?=$var;?>" />
        <label id="err_<?=$var;?>"></label>
      <p class="pw01">パスワード</p>
		<? $var = "shop_login_pw";?>
        <input type="password" name="<?=$var;?>" id="<?=$var;?>" />
        <label id="err_<?=$var;?>"></label>
    </div>
    
    <div class="login_box02">
			<? $var = "customer_id";?>
			<input type="hidden" name="<?=$var;?>" id="<?=$var;?>" value="<?=$$var;?>" />
			
      <input type="submit" id="form_confirm" name="form_confirm" value="ログイン" />
    </div>
    </form>
  </div>
</div>
<!--コンテンツエリア-->

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer.php"; ?>
<!--フッターエリア-->

</div>
</body>
</html>