<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
	
	if($_COOKIE['cook_gameiten'] != '')
	{
		$sql = "select shop_id, shop_name, img_1 from app_shop where login_cookie ='".$_COOKIE['cook_gameiten']."'";

		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$shop_id = $db_result[0]["shop_id"];
			$shop_name = $db_result[0]["shop_name"];
			$h1_title = $db_result[0]["shop_name"];
			$img_1 = $db_result[0]["img_1"];
		}
	}
	
	if($shop_id=="raikamu")
	{
		$view_price_person = "人数";
		$view_yen_person = "人";
	}
	else
	{
		$view_price_person = "金額";
		$view_yen_person = "円";
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>売上管理</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/common.css" media="all" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/admin.css" />
<link rel="stylesheet" type="text/css" href="/app_management/common/css/form.css" />

<!-- ↓↓jQuery↓↓ -->
<script type="text/javascript" src="/app_management/js/jquery.js"></script>
<!-- ↑↑jQuery↑↑ -->

<!-- ↓↓accordion menu↓↓ -->
<script type="text/javascript" src="/app_management/js/accordion.js"></script>
<!-- ↑↑accordion menu↑↑ -->

<script language="javascript"> 
	function fnChangeSel(i) { 
		var result = confirm('キャンセルに変更しますか？'); 
		if(result){ 
			document.location.href = './shop_uriage_status_up.php?shop_uriage_id='+i;
		} 
	}
</script>
<script type="text/javascript">
	$(function() {
		$('#form_confirm').click(function() {
			err_default = "";
			err_check_count = 0;
			err_check = false;
			bgcolor_default = "#FFFFFF";
			bgcolor_err = "#FFCCCC";
			background = "background-color";

			err_check_count += check_input("price");
			
			if(err_check_count)
			{
				alert("入力に不備があります");
				return false;
			}
			else
			{
				$('#form_confirm').submit();
				return true;
			}
			
			
		});
		
		function check_input($str) 
		{
			$("#err_"+$str).html(err_default);
			$("#"+$str).css(background,bgcolor_default);
			
			if($('#'+$str).val()=="")
			{
				err ="<br /><span style='color:#F00'>正しく入力してください。</span>";
				$("#err_"+$str).html(err);
				$("#"+$str).css(background,bgcolor_err);
				
				return 1;
			}
			return 0;
		}
		
	});
//-->
</script>
</head>

<body>
<?
	//ショップチェック
	$common_connect -> Fn_shop_check($shop_name);
	$cook_gameiten = $_COOKIE['cook_gameiten'];
	
	foreach($_GET as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//ショップ情報
	$sql = "select shop_name, shop_id from app_shop where login_cookie='".$cook_gameiten."' ";  
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$shop_name = $db_result[0]["shop_name"];
		$shop_id = $db_result[0]["shop_id"];
	}
		
?>
<div>
<div id="container">

<!--ヘッダーエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/header_in.php"; ?>
<!--ヘッダーエリア-->

<!--サーチナビエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/shop/include/searchnav.php"; ?>
<!--サーチナビエリア-->

<div id="sec_cont_outer" class="clearfix">
	<!--サイドナビエリア-->
	<div id="sidenav">
		<div class="mainnav clearfix">
		<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/mainnav/side_gameiten.php"; ?>
		</div>
	</div>
	<!--サイドナビエリア-->
	
	<!--第二階層メインコンテンツ-->
	<div id="sec_maincontents">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" valign="top">

				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>売上管理</p>
					</div>
				</div>
        <br />
        <section id="form_regist" class="clearfix">
        <table cellpadding="5" cellspacing="1" class="common_table input_table">
          <tr>
            <th>加盟店</th>
            <td>
							<?php $var = "shop_name";?>
              <?php echo $$var;?>
            </td>
          </tr>
        <?
					$sql = "SELECT count(shop_uriage_id) as today_count, sum(price) as today_sum FROM app_shop_uriage ";
					$sql .= " where shop_id='".$shop_id."' ";
					$sql .= " and status<'90' ";
					$sql .= " and left(regi_date, 10) = '".date("Y-m-d")."' ";

					$db_result = $common_dao->db_query($sql);
					if($db_result)
					{
						$today_count = $db_result[0]["today_count"];
						$today_sum = $db_result[0]["today_sum"];
					}
				?>
          <tr>
            <th>本日の売り上げ<br />(<? echo date("Y-m-d")?>)</th>
            <td>
            		<span style="font-size:16px; color:#00F;"><? echo number_format($today_sum);?></span><? echo $view_yen_person; ?>（<? echo number_format($today_count);?>件）
            </td>
          </tr>
          
        <?
					$sql = "SELECT count(shop_uriage_id) as month_count, sum(price) as month_sum FROM app_shop_uriage ";
					$sql .= " where shop_id='".$shop_id."' ";
					$sql .= " and status<'90' ";
					$sql .= " and left(regi_date, 7) = '".date("Y-m")."' ";

					$db_result = $common_dao->db_query($sql);
					if($db_result)
					{
						$month_count = $db_result[0]["month_count"];
						$month_sum = $db_result[0]["month_sum"];
					}
				?>
          <tr>
            <th>今月の売り上げ<br />(<? echo date("Y年m月")?>)</th>
            <td>
            		<span style="font-size:16px; color:#00F;"><? echo number_format($month_sum);?></span><? echo $view_yen_person; ?>（<? echo number_format($month_count);?>件）
            </td>
          </tr>
        </table>
        
        </section>
        <br />
        <br />
        <br />
      
				<div class="pub_title01">
					<div class="pub_title01_inner">
						<p>検索</p>
					</div>
				</div>
        <br />
        <form action="<?=$_SERVER["PHP_SELF"];?>#form_search" method="GET" name="search" style="margin:0px;" id="form_search">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table search_table">
          <tr>
            <th>キーワード</th>
            <td>
							<?php $var = "s_keyword";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
          <tr>
            <th>顧客ID</th>
            <td>
							<?php $var = "s_customer_id";?>
              <input type="text" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?=$$var;?>" />
            </td>
          </tr>
          <tr>
            <th>登録年月</th>
            <td>
<?
	$sql = "SELECT DATE_FORMAT(regi_date, '%Y%m') as yyyymm FROM app_shop_uriage " ;
	$sql .= " where shop_id='".$shop_id."' ";
	$sql .= " group by DATE_FORMAT(regi_date, '%Y%m') ";
	$sql .= " order by DATE_FORMAT(regi_date, '%Y%m') desc";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
		{
			$arr_s_yyyymm[] = $db_result[$db_loop]["yyyymm"];
		}
	}
						?>
							<?php $var = "s_yyyymm";?>
              <select name="<? echo $var;?>" id="<? echo $var;?>">
              	<option value="">---</option>
              	<?
                foreach($arr_s_yyyymm as $key=>$value)
								{
								?>
									<option value="<? echo $value;?>" <? if($value==$$var) { echo " selected ";}?>><? echo substr($value, 0, 4)."年".substr($value, 4, 2)."月";?></option>
								<?
								}
								?>
              
              </select>
            </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="common_table_button search_table_confirm">
          <tr>
            <td>
            	<input type="submit" value="検索する" />
            </td>
          </tr>
        </table>
        </form>
        <br />
      
<?php
	
	$view_count=50;   // List count
	$offset=0;

	if(!$page)
	{
		$page=1;
	}
	Else
	{
		$offset=$view_count*($page-1);
	}
	
	$where = "";
	if($s_keyword != "")
	{
		$where .= " and (customer_name like '%".$s_keyword."%' or customer_name_en like '%".$s_keyword."%') ";
	}
	if($s_yyyymm != "")
	{
		$where .= " and DATE_FORMAT(regi_date, '%Y%m')='".$s_yyyymm."' ";
	}
	
	if($s_customer_id != "")
	{
		$where .= " and customer_id='".$s_customer_id."' ";
	}
	
	
	
	$where .= " and shop_id='".$shop_id."' ";


	//合計
	$sql_count = "SELECT count(shop_uriage_id) as all_count FROM app_shop_uriage where 1 ".$where ;
	
	$db_result_count = $common_dao->db_query($sql_count);
	if($db_result_count)
	{
		$all_count = $db_result_count[0]["all_count"];
	}
	
	//リスト表示
	$arr_db_field = array("shop_uriage_id", "shop_id", "shop_name", "customer_id", "customer_name", "customer_name_en", "price", "comment");
	$arr_db_field = array_merge($arr_db_field, array("regi_date", "status"));
	
	$sql = "SELECT ";
	foreach($arr_db_field as $val)
	{
		$sql .= $val.", ";
	}
	$sql .= " 1 FROM app_shop_uriage where 1 ".$where ;
	if($order_name != "")
	{
		$sql .= " order by ".$order_name." ".$order;
	}
	else
	{
		$sql .= " order by shop_uriage_id desc";
	}
	$sql .= " limit $offset,$view_count";
	
	$db_result = $common_dao->db_query($sql);
	if($db_result)
	{
		$inner_count = count($db_result);
?>
      <div id="search_list">
        すべて：<?=$all_count;?>
        <table width="100%" border="0" cellpadding="6" cellspacing="1" class="table02">
          <tr align="center">
            <th width="70">編集</th>
            <th>売上ID</th>
            <th>顧客名</th>
            <th>英語名</th>
            <th><? echo $view_price_person;?></th>
            <th>ステータス</th>
            <th>登録日</th>
          </tr>
          <tr align="center">
            <td>ソート</td>
            <td><a href="?order_name=shop_uriage_id&order=desc">▼</a>｜<a href="?order_name=shop_uriage_id">▲</a></td>
            <td><a href="?order_name=customer_name&order=desc">▼</a>｜<a href="?order_name=customer_name">▲</a></td>
            <td><a href="?order_name=customer_name_en&order=desc">▼</a>｜<a href="?order_name=customer_name_en">▲</a></td>
            <td><a href="?order_name=price&order=desc">▼</a>｜<a href="?order_name=price">▲</a></td>
            <td><a href="?order_name=status&order=desc">▼</a>｜<a href="?order_name=status">▲</a></td>
            <td><a href="?order_name=regi_date&order=desc">▼</a>｜<a href="?order_name=regi_date">▲</a></td>
          </tr>
<?php
	for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
	{
		foreach($arr_db_field as $val)
		{
			$$val = $db_result[$db_loop][$val];
		}
?>
          <tr align="center">
            <td>
            <?php if($status!="99") { ?>
              <a href="#" onClick='fnChangeSel("<?php echo $shop_uriage_id;?>");'>キャンセル</a>
            <? } ?>
            </td>
            <td><?php echo $shop_uriage_id;?></td>
            <td><?php echo $customer_name;?></td>
            <td><?php echo $customer_name_en;?></td>
            <td><?php echo number_format($price); if($status<"90") { $sum_price+=$price; $sum_count++;} else { $sum_cancel_price+=$price; $sum_cancel_count++;} ?></td>
            <td><span style="color:#F00"><?php if($status=="99") {echo "キャンセル";}?></span></td>
            <td><?php echo $regi_date;?></td>
          </tr>
<?php
	}
?>
          <tr align="center">
          	<td></td>
          	<td style="background-color: rgb(255, 180, 0);" colspan="7">
            	金額売上：<?php echo number_format($sum_price);?><? echo $view_yen_person;?>（<?php echo number_format($sum_count);?>件）　
              キャンセル：<?php echo number_format($sum_cancel_price);?><? echo $view_yen_person;?>（<?php echo number_format($sum_cancel_count);?>件）
            </td>
          </tr>
        </table>

        <div style="text-align:center;">
          <?php $common_connect -> Fn_paging($view_count, $all_count); ?>
        </div><!-- //center-->
      </div><!-- //search_list-->
<?
	} //$db_result
?>


        </td>
      </tr>
    </table>
	</div>
	<!--第二階層メインコンテンツ-->
</div>

<!--フッターエリア-->
<? require_once $_SERVER['DOCUMENT_ROOT']."/app_management/include/footer_in.php"; ?>
<!--フッターエリア-->

<!-------end---------->

  </div><!--container-->
</div>
</body>
</html>