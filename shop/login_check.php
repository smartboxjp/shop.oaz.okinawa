<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<title>管理サイト</title>
</head>
<body>
<?
	
	$customer_id = trim($_POST['customer_id']);
	$shop_login_id = trim($_POST['shop_login_id']);
	$shop_login_pw = trim($_POST['shop_login_pw']);

	if ($shop_login_id == "" or $shop_login_pw == "") 
	{
	    $common_connect -> Fn_javascript_back("IDとパスワードを確認ください。");
	}
	else
	{
		$sql = "select shop_id, shop_name from app_shop where shop_login_id ='$shop_login_id' and shop_login_pw='$shop_login_pw'";

		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$db_shop_id = $db_result[0]["shop_id"];
			$db_shop_name = $db_result[0]["shop_name"];
		}
		
		if (trim($db_shop_id) != "")
		{
			//管理者の場合
			session_start();
			$_SESSION['shop_id']=$db_shop_id;
			$_SESSION['shop_name']=$db_shop_name;
			
			$login_cookie = sha1($shop_login_id."_".$shop_login_pw);
			$db_insert = "update app_shop set login_cookie='".$login_cookie."' ";
			$db_insert .= " where shop_id='".$db_shop_id."'";
			$db_result = $common_dao->db_update($db_insert);
	
			setcookie("cook_gameiten", $login_cookie, time()+3600*24*365, "/");//暗号化してクッキーに保存
			
			if($customer_id=="")
			{
				$url = "/shop/shop_uriage/shop_uriage_list.php";
			}
			else
			{
				$url = "/shop/shop_uriage/shop_uriage_list.php?customer_id=".$customer_id;
			}
			$common_connect -> Fn_redirect($url);
		}
		else
		{
			$common_connect -> Fn_javascript_back("IDとPWを確認してください。");
		}

	}
?>
</body>
</html>